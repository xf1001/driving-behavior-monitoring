package com.ipcamera.detect;
import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import vstc2.nativecaller.NativeCaller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;


import com.ipcamer.detect.R;
import com.ipcamera.detect.BridgeService.PlayInterface;
import com.ipcamera.detect.adapter.ViewPagerAdapter;
import com.ipcamera.detect.utils.AudioPlayer;
import com.ipcamera.detect.utils.ContentCommon;
import com.ipcamera.detect.utils.CustomAudioRecorder;
import com.ipcamera.detect.utils.CustomAudioRecorder.AudioRecordResult;
import com.ipcamera.detect.utils.CustomBuffer;
import com.ipcamera.detect.utils.CustomBufferData;
import com.ipcamera.detect.utils.CustomBufferHead;
import com.ipcamera.detect.utils.DrawCaptureRect;
import com.ipcamera.detect.utils.MyRender;
import com.ipcamera.detect.utils.MySharedPreferenceUtil;
import com.ipcamera.detect.utils.SystemValue;
import com.ipcamera.detect.utils.VideoFramePool;

import com.tenginekit.AndroidConfig;
import com.tenginekit.KitCore;
import com.tenginekit.face.Face;
import com.tenginekit.face.FaceDetectInfo;
import com.tenginekit.face.FaceIrisInfo;
import com.tenginekit.face.FaceLandmark3dInfo;
import com.tenginekit.face.FaceLandmarkInfo;
import com.tenginekit.model.TenginekitPoint;

import com.ipcamera.detect.model.TensorFlowObjectDetectionAPIModel;
/**
 * 预览界面的活动
 * @author Liu Huanxi
 * @Date 2021/6/5
 * 文件名：PlayActivity.java
 * @Description 预览模式的活动类，检测驾驶员行为
 **/

public class PlayActivity extends Activity implements OnTouchListener,OnGestureListener, OnClickListener, PlayInterface ,AudioRecordResult ,BridgeService.CameraLightInterfaceInterface {
	//调试时用到的TAG
	private static final String LOG_TAG = "PlayActivity";

	/**疲劳检测请求的msg_index*/
	protected static final int FATIGUE_MSG = 137;
	/**偏头检测请求的msg_index*/
	protected static final int NOD_MSG = 138;
	/**初始化请求的msg_index*/
	protected static final int INIT_MSG = 139;
	/**子线程开启的用于检测抽烟、打电话的handle名称*/
	private static final String HANDLE_THREAD_NAME = "cameraBackground";

	/**定时处理检测消息的Handler和timer*/
	private Handler timeHandler;
	Timer timer4Fatigue = null;
	Timer timer4Nod = null;
	Timer timer4Init = null;

	/**轻度疲劳连续出现的时间阙值*/
	protected int twiceTimes = 2;
	/**疲劳程度*/
	protected double fatigueDegree = 0;
	/**轻度疲劳程度的阈值*/
	protected static double fatigueThreshold1 = 0.23;
	/**中度疲劳程度的阈值*/
	protected static double fatigueThreshold2 = 0.69;
	/**重度疲劳程度的阈值*/
	protected static double fatigueThreshold3 = 1.61;
	/**特别疲劳程度的阈值*/
	protected static double fatigueThreshold4 = 2.3;

	/**判定闭眼的阈值，PERCLOS测量使用P70标准*/
	protected static double eyesCloseThreshold = 0.7;
	/**PERCLOS的值*/
	protected double perclosDegree = 0;
	/**1分钟平均闭眼时间*/
	protected double avgBlinkTime = 0;
	/**1分钟闭眼的帧数*/
	protected int eyesCloseNumber = 0;
	/**1分钟总帧数*/
	protected int totalFrameNumber = 0;
	//*1分钟眨眼次数*/
	protected int blinkNumber = 0;
	/**是否处于闭眼*/
	protected boolean isEyesClose = false;

	/**采用二次判决的方式判定打哈欠
	1分钟打哈欠的个数*/
	protected int yawnNumber = 0;
	/**判断哈欠的第一个阙值*/
	protected static double yawnThreshold1 = 0.65;
	/**判断哈欠的第二个阙值*/
	protected static double yawnThreshold2 = 0.5;
	/**哈欠持续的帧判定阙值，本设备设置的帧数是15帧/s */
	protected static double yawnFrameThreshold1 = 15;
	protected static double yawnFrameThreshold2 = 45;
	protected static double yawnFrameThreshold3 = 75;
	/**哈欠程度是否处于第一个阙值和第二个之间，即小哈欠*/
	protected boolean isSmallYawn = false;
	/**哈欠程度是否处于大于第一个阙值，即打哈欠*/
	protected boolean isBigYawn = false;
	/**连续大于第一个阈值，加1*/
	protected int yawnFrame1 = 0;
	/**连续大于第二个阙值，加1*/
	protected int yawnFrame2 = 0;
	/**大哈欠连续最大帧数目*/
	protected int maxYawnFrame1 = 0;
	/**小哈欠连续最大帧数目*/
	protected int maxYawnFrame2 = 0;

	/**普通情况时头部姿态参数，分别代表了头在三个方向转向的度数初始化过程中得到*/
	protected float normalPitch = 0;
	protected float normalRoll = 0;
	protected float normalYaw = 0;

	/**初始化normalPitch normalRoll normalYaw用到的，用来计算前15s的平均值*/
	protected float totalPitch = 0;
	protected float totalRoll = 0;
	protected float totalYaw = 0;
	/**是否需要初始化头部姿态*/
	protected boolean initPosition = true;
	/**初始化所需的帧数*/
	protected int initFrame = 0;

	/**一段时间内偏头的帧数*/
	protected int headNodFrame = 0;
	/**一段时间内不偏头的帧数*/
	protected int headNormalFrame = 0;

	/**注意力的阈值*/
	protected static double noticeThreshold1 = 0.45;
	protected static double noticeThreshold2 = 0.65;
	/**连续失去注意力的帧数*/
	private int loseNoticeFrame = 0;

	/**连续未检测出人的帧数*/
	private int notDetectFrame = 0;
	/**判断初始化过程中是否检测不到人*/
	private boolean fail2Init = false;

	/** 摄像头返回的视频流宽*/
	protected int previewWidth;
	/** 摄像头返回的视频流高*/
	protected int previewHeight;
	/** 平面展示的宽*/
	protected static double ScreenWidth;
	/** 平面展示的高*/
	protected static double ScreenHeight;

	/** 摄像头返回的数据流*/
	protected byte[] mNV21Bytes = null;
	/**获取数据流的子线程*/
	protected Runnable imageConverter;

	/**人脸矩形框对象*/
	protected DrawCaptureRect mFaceRect = null;

	/**警报提示播放器*/
	protected MediaPlayer alarmPlayer = null;
	/**警报完成监听器*/
	protected MediaPlayer.OnCompletionListener onCompletionListener;
	/**判定是否在警报*/
	protected boolean isAlarming = false;

	/**判断抽烟和接电话的子线程*/
	private HandlerThread backgroundThread;
	/**
	 * A {@link Handler} 对应上方子线程的Handler
	 */
	private Handler backgroundHandler;

	/**同步锁，重复的分类计算*/
	private final Object lock = new Object();
	/**是否在进行分类计算*/
	private boolean runClassifier = false;
	/**判断吸烟和接电话的分类器*/
	private Classifier classifier;
	/**分类器对应模型文件*/
	private static final String TF_OD_API_MODEL_FILE = "file:///android_asset/frozen_inference_graph_v6.pb";
	/**分类器对应的LABEL列表*/
	private static final String TF_OD_API_LABELS_FILE = "file:///android_asset/coco_labels_list.txt";
	/**分类输入的位图的大小*/
	private static final int TF_OD_API_INPUT_SIZE = 300;
	/**分类结果的致信度的阙值*/
	private static final float MINIMUM_CONFIDENCE_TF_OD_API = 0.6f;

	private static final int AUDIO_BUFFER_START_CODE = 0xff00ff;
	/**surfaceView控键*/
	private GLSurfaceView playSurface = null;
	/**GLSurfaceView的渲染器*/
	private MyRender myRender;
	/**
	 * videodata：视频流数据
	 * videoDataLen：视频流长度
	 * nVideoWidths：视频的宽
	 * nVideoHeights：视频的高
	 */
	private byte[] videodata = null;
	private int videoDataLen = 0;
	public int nVideoWidths = 0;
	public int nVideoHeights = 0;

	private View progressView = null;//滚动圈视图
	private boolean bProgress = true;
	private GestureDetector gt = new GestureDetector(this);//手势监听器
	private final int BRIGHT = 1;//亮度标志 
	private final int CONTRAST = 2;//对比度标志
	private final int IR_STATE = 14;//IR(夜视)开关
	private int nResolution = 0;//分辨率值
	private int nBrightness = 0;//亮度值
	private int nContrast = 0;//对比度

	private boolean bInitCameraParam = false;
	private boolean bManualExit = false;
	private TextView textosd = null;//顶部textView，显示用户名的
	private String strName = null;//用户名
	private String strDID = null;//设备id
	private View osdView = null;
	private boolean bDisplayFinished = true;
	private CustomBuffer AudioBuffer = null;
	private AudioPlayer audioPlayer = null;
	private boolean bAudioStart = false;

	private boolean isUpDownPressed = false;
	private ImageView videoViewPortrait;
	private ImageView videoViewStandard;
	//底部控件声明
	private HorizontalScrollView bottomView;
	//下面是一系列功能按钮
	private ImageButton ptzAudio, ptztalk, ptzDefaultSet, ptzBrightness, ptzContrast, ptzTake_photos, ptzTake_vodeo, ptzResolutoin, preset;
	private int nStreamCodecType;//分辨率格式

	private PopupWindow controlWindow;//设备方向控制提示控件
	private PopupWindow mPopupWindowProgress;//进度条控件
	private PopupWindow presetBitWindow;//预置位面板
	private PopupWindow resolutionPopWindow;//分辨率面板
	//上下左右提示文本
	private TextView control_item;
	//正在控制设备
	private boolean isControlDevice = false;

	private String stqvga = "qvga";
	private String stvga = "vga";
	private String stqvga1 = "qvga1";
	private String stvga1 = "vga1";
	private String stp720 = "p720";
	private String sthigh = "high";
	private String stmiddle = "middle";
	private String stmax = "max";

	//预位置设置
	private Button[] btnLeft = new Button[16];
	private Button[] btnRigth = new Button[16];
	private ViewPager prePager;
	private List<View> listViews;
	//分辨率标识符
	private boolean ismax = false;
	private boolean ishigh = false;
	private boolean isp720 = false;
	private boolean ismiddle = false;
	private boolean isqvga1 = false;
	private boolean isvga1 = false;
	private boolean isqvga = false;
	private boolean isvga = false;

	private Animation showAnim;
	private boolean isTakepic = false;//是否需要拍照
	private boolean isPictSave = false;//是否需要存照片
	private boolean isTalking = false;//是否在说话
	private boolean isMcriophone = false;//是否打开麦克风
	public boolean isH264 = false;//是否是H264格式标志
	public boolean isJpeg = false;//图片是否为Jpeg格式
	private boolean isTakeVideo = false;//是否在录像
	private long videotime = 0;// 录每张图片的时间

	private Animation dismissAnim;
	private int timeTag = 0;
	private int timeOne = 0;
	private int timeTwo = 0;
	private BitmapDrawable drawable = null;
	private boolean bAudioRecordStart = false;
	//送话器
	private CustomAudioRecorder customAudioRecorder;

	private ImageButton lightBtn, sireBtn;
	private int picNum = 0;//拍照张数标识
	/**
	 * @Description 弹出退出确定的对话框
	 * @Throws：空指针错误
	 * NullPointerException	dialog分配内存失败
	 */
	public void showSureDialog() {
		AlertDialog.Builder builder = null;
		try{
			builder = new AlertDialog.Builder(this);
		}catch (NullPointerException np){
			Log.e(LOG_TAG, "showSureDialog: builder is null");
			return;
		}
		builder.setIcon(R.drawable.app);
		builder.setTitle(getResources().getString(R.string.exit)
				+ getResources().getString(R.string.app_name));
		builder.setMessage(R.string.exit_alert);
		builder.setPositiveButton(R.string.str_ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Intent intent = new Intent("finish");
						sendBroadcast(intent);
						PlayActivity.this.finish();
					}
				});
		builder.setNegativeButton(R.string.str_cancel, null);
		builder.show();
	}

	/**设置灯与警笛的视图（改软件系统的摄像头无法配置）*/
	@SuppressLint("HandlerLeak")
	private Handler deviceParamsHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case 11:
					lightBtn.setBackground(getResources().getDrawable(R.drawable.camera_light_btn_off));
					break;
				case 12:
					lightBtn.setBackground(getResources().getDrawable(R.drawable.camera_light_btn_on));
					break;
				case 13:
					sireBtn.setBackground(getResources().getDrawable(R.drawable.camera_siren_btn_off));
					break;
				case 14:
					sireBtn.setBackground(getResources().getDrawable(R.drawable.camera_siren_btn_on));
					break;
				default:
					break;
			}
		}
	};

	//默认视频参数
	private void defaultVideoParams() {
		nBrightness = 1;//设置亮度
		nContrast = 128;//设置对比度
		NativeCaller.PPPPCameraControl(strDID, 1, 0);
		NativeCaller.PPPPCameraControl(strDID, 2, 128);
		showToast(R.string.ptz_default_vedio_params);
	}

	private void showToast(int i) {
		Toast.makeText(PlayActivity.this, i, Toast.LENGTH_SHORT).show();
	}

	//设置视频可见
	private void setViewVisible() {
		if (bProgress) {
			bProgress = false;
			progressView.setVisibility(View.INVISIBLE);
			osdView.setVisibility(View.VISIBLE);
			getCameraParams();
		}
	}

	int disPlaywidth;
	private Bitmap mBmp;
	//传输视频流回调的Handler
	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == 1 || msg.what == 2) {
				setViewVisible();
			}
			if (!isPTZPrompt) {
				isPTZPrompt = true;
				showToast(R.string.ptz_control);
			}
			int width = getWindowManager().getDefaultDisplay().getWidth();
			int height = getWindowManager().getDefaultDisplay().getHeight();
			switch (msg.what) {
				case 1: // h264的视频流回调后触发，对视频的显示做了一些设置
				{
					if (reslutionlist.size() == 0) {
						if (nResolution == 0) {
							ismax = true;
							ismiddle = false;
							ishigh = false;
							isp720 = false;
							isqvga1 = false;
							isvga1 = false;
							addReslution(stmax, ismax);
						} else if (nResolution == 1) {
							ismax = false;
							ismiddle = false;
							ishigh = true;
							isp720 = false;
							isqvga1 = false;
							isvga1 = false;
							addReslution(sthigh, ishigh);
						} else if (nResolution == 2) {
							ismax = false;
							ismiddle = true;
							ishigh = false;
							isp720 = false;
							isqvga1 = false;
							isvga1 = false;
							addReslution(stmiddle, ismiddle);
						} else if (nResolution == 3) {
							ismax = false;
							ismiddle = false;
							ishigh = false;
							isp720 = true;
							isqvga1 = false;
							isvga1 = false;
							addReslution(stp720, isp720);
							nResolution = 3;
						} else if (nResolution == 4) {
							ismax = false;
							ismiddle = false;
							ishigh = false;
							isp720 = false;
							isqvga1 = false;
							isvga1 = true;
							addReslution(stvga1, isvga1);
						} else if (nResolution == 5) {
							ismax = false;
							ismiddle = false;
							ishigh = false;
							isp720 = false;
							isqvga1 = true;
							isvga1 = false;
							addReslution(stqvga1, isqvga1);
						}
					} else {
						if (reslutionlist.containsKey(strDID)) {
							getReslution();
						} else {
							if (nResolution == 0) {
								ismax = true;
								ismiddle = false;
								ishigh = false;
								isp720 = false;
								isqvga1 = false;
								isvga1 = false;
								addReslution(stmax, ismax);
							} else if (nResolution == 1) {
								ismax = false;
								ismiddle = false;
								ishigh = true;
								isp720 = false;
								isqvga1 = false;
								isvga1 = false;
								addReslution(sthigh, ishigh);
							} else if (nResolution == 2) {
								ismax = false;
								ismiddle = true;
								ishigh = false;
								isp720 = false;
								isqvga1 = false;
								isvga1 = false;
								addReslution(stmiddle, ismiddle);
							} else if (nResolution == 3) {
								ismax = false;
								ismiddle = false;
								ishigh = false;
								isp720 = true;
								isqvga1 = false;
								isvga1 = false;
								addReslution(stp720, isp720);
								nResolution = 3;
							} else if (nResolution == 4) {
								ismax = false;
								ismiddle = false;
								ishigh = false;
								isp720 = false;
								isqvga1 = false;
								isvga1 = true;
								addReslution(stvga1, isvga1);
							} else if (nResolution == 5) {
								ismax = false;
								ismiddle = false;
								ishigh = false;
								isp720 = false;
								isqvga1 = true;
								isvga1 = false;
								addReslution(stqvga1, isqvga1);
							}
						}
					}

					if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
						FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
								width, width * 3 / 4);
						lp.gravity = Gravity.CENTER;
						playSurface.setLayoutParams(lp);
					} else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
						FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
								width, height);
						lp.gravity = Gravity.CENTER;
						playSurface.setLayoutParams(lp);
					}
					//将视频流存储在视频帧池里
					framePool.pushBytes(videodata, videoDataLen, nVideoWidths, nVideoHeights);
					//myRender.writeSample(videodata, nVideoWidths, nVideoHeights);
				}
				break;
				case 2: // 回调的是JPEG格式，则构建相应的位图并存储
				{
					if (reslutionlist.size() == 0) {
						if (nResolution == 1) {
							isvga = true;
							isqvga = false;
							addReslution(stvga, isvga);
						} else if (nResolution == 0) {
							isqvga = true;
							isvga = false;
							addReslution(stqvga, isqvga);
						}
					} else {
						if (reslutionlist.containsKey(strDID)) {
							getReslution();
						} else {
							if (nResolution == 1) {
								isvga = true;
								isqvga = false;
								addReslution(stvga, isvga);
							} else if (nResolution == 0) {
								isqvga = true;
								isvga = false;
								addReslution(stqvga, isqvga);
							}
						}
					}
					//解码视频流
					try{
						mBmp = BitmapFactory.decodeByteArray(videodata, 0, videoDataLen);
					}catch (NullPointerException np){
						Log.e(LOG_TAG, "handleMessage: fail to decode Byte Array" );
					}
					if (mBmp == null) {
						bDisplayFinished = true;
						return;
					}
					if (isTakepic) {
						takePicture(mBmp);
						isTakepic = false;
					}
					nVideoWidths = mBmp.getWidth();
					nVideoHeights = mBmp.getHeight();

					if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
						// 创建位图
						Bitmap bitmap = Bitmap.createScaledBitmap(mBmp, width,
								width * 3 / 4, true);
						//videoViewLandscape.setVisibility(View.GONE);
						videoViewPortrait.setVisibility(View.VISIBLE);
						videoViewPortrait.setImageBitmap(bitmap);

					} else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
						Bitmap bitmap = Bitmap.createScaledBitmap(mBmp, width, height, true);
						videoViewPortrait.setVisibility(View.GONE);
						//videoViewLandscape.setVisibility(View.VISIBLE);
						//videoViewLandscape.setImageBitmap(bitmap);
					}
				}
				break;
				default:
					break;
			}
			if (msg.what == 1 || msg.what == 2) {
				bDisplayFinished = true;
			}
		}

	};
	//获得摄像头的一些设置
	private void getCameraParams() {
		NativeCaller.PPPPGetSystemParams(strDID,
				ContentCommon.MSG_TYPE_GET_CAMERA_PARAMS);
	}

	//设备消息的处理器，响应设备断开连接
	@SuppressLint("HandlerLeak")
	private Handler msgHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				Log.d("tag", "断线了");
				Toast.makeText(getApplicationContext(),
						R.string.pppp_status_disconnect, Toast.LENGTH_SHORT)
						.show();
				finish();
			}
		}
	};
	//视频流帧池，存储视频流，保证以一个稳定的帧数输出
	private VideoFramePool framePool;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// getDataFromOther();

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.play);
		//设置音频播放器
		onCompletionListener = new MediaPlayer.OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				isAlarming = false;
			}
		};
		if (alarmPlayer != null) {
			alarmPlayer.release();
		}
		alarmPlayer = MediaPlayer.create(this, R.raw.first_init);
		alarmPlayer.setOnCompletionListener(onCompletionListener);
		alarmPlayer.start();
		isAlarming = true;
		showToast(R.string.initing);
		//脸部框图初始化
		mFaceRect = new DrawCaptureRect(getApplicationContext(),
				190, 10, 1, 1, getResources().getColor(R.color.color_green));
		//在一个activity上面添加额外的content（脸部框图）
		addContentView(mFaceRect, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
		try {
			classifier = TensorFlowObjectDetectionAPIModel.create(
					this.getAssets(), TF_OD_API_MODEL_FILE, TF_OD_API_LABELS_FILE, TF_OD_API_INPUT_SIZE);
		} catch (IOException e) {
			Log.e(LOG_TAG, "Failed to initialize an image classifier.");
		}

		strName = SystemValue.deviceName;
		strDID = SystemValue.deviceId;
		BridgeService.setCameraLightInterfaceInterface(PlayActivity.this);
		disPlaywidth = getWindowManager().getDefaultDisplay().getWidth();
		findView();
		AudioBuffer = new CustomBuffer();
		audioPlayer = new AudioPlayer(AudioBuffer);
		customAudioRecorder = new CustomAudioRecorder(this);
		BridgeService.setPlayInterface(this);
		NativeCaller.StartPPPPLivestream(strDID, 10, 1);//确保不能重复start

		getCameraParams();
		dismissTopAnim = AnimationUtils.loadAnimation(this,
				R.anim.ptz_top_anim_dismiss);
		showTopAnim = AnimationUtils.loadAnimation(this,
				R.anim.ptz_top_anim_show);
		showAnim = AnimationUtils.loadAnimation(this,
				R.anim.ptz_otherset_anim_show);
		dismissAnim = AnimationUtils.loadAnimation(this,
				R.anim.ptz_otherset_anim_dismiss);

		myRender = new MyRender(playSurface);
		framePool = new VideoFramePool(playSurface, myRender);
		framePool.setFrameRate(15);
		framePool.start();
		playSurface.setRenderer(myRender);

		showBottom();

		//灯与警笛（改软件系统的摄像头无法配置）
		if (SystemValue.supportLightAndSirenO13AndO10(MySharedPreferenceUtil.getSystemVer(this, strDID))) {
			getLightAndSirenStatte(strDID, SystemValue.devicePass);
			lightBtn.setVisibility(View.VISIBLE);
			sireBtn.setVisibility(View.VISIBLE);
		} else {
			lightBtn.setVisibility(View.GONE);
			sireBtn.setVisibility(View.GONE);
		}
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (mPopupWindowProgress != null && mPopupWindowProgress.isShowing()) {
			mPopupWindowProgress.dismiss();
		}
		if (resolutionPopWindow != null && resolutionPopWindow.isShowing()) {
			resolutionPopWindow.dismiss();
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			if (!bProgress) {
				Date date = new Date();
				if (timeTag == 0) {
					timeOne = date.getSeconds();
					timeTag = 1;
					showToast(R.string.main_show_back);
				} else if (timeTag == 1) {
					timeTwo = date.getSeconds();
					if (timeTwo - timeOne <= 3) {
						Intent intent = new Intent("finish");
						sendBroadcast(intent);
						PlayActivity.this.finish();
						timeTag = 0;
					} else {
						timeTag = 1;
						showToast(R.string.main_show_back);
					}
				}
			} else {
				showSureDialog();
			}
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (!bProgress) {
				showBottom();
			} else {
				showSureDialog();
			}
			framePool.exit();
		}
		return super.onKeyDown(keyCode, event);
	}

	protected void setResolution(int Resolution) {
		Log.d("tag", "setResolution resolution:" + Resolution);
		NativeCaller.PPPPCameraControl(strDID, 16, Resolution);
	}

	/**
	 * 配置视图，绑定其控件
	 */
	private void findView() {
		//方向控制提示框
		initControlDailog();
		//视频渲染画面控件
		playSurface = (GLSurfaceView) findViewById(R.id.mysurfaceview);
		playSurface.setOnTouchListener(this);
		playSurface.setLongClickable(true);//确保手势识别正确工作

		videoViewPortrait = (ImageView) findViewById(R.id.vedioview);
		videoViewStandard = (ImageView) findViewById(R.id.vedioview_standard);

		progressView = (View) findViewById(R.id.progressLayout);
		//顶部
		osdView = (View) findViewById(R.id.osdlayout);

		//显示设备名称
		textosd = (TextView) findViewById(R.id.textosd);
		textosd.setText(strName);
		textosd.setVisibility(View.VISIBLE);


		//底部菜单
		bottomView = (HorizontalScrollView) findViewById(R.id.bottom_view);

		ptztalk = (ImageButton) findViewById(R.id.ptz_talk);
		ptzAudio = (ImageButton) findViewById(R.id.ptz_audio);
		ptzTake_photos = (ImageButton) findViewById(R.id.ptz_take_photos);
		ptzTake_vodeo = (ImageButton) findViewById(R.id.ptz_take_videos);
		ptzDefaultSet = (ImageButton) findViewById(R.id.ptz_default_set);
		ptzBrightness = (ImageButton) findViewById(R.id.ptz_brightness);
		ptzContrast = (ImageButton) findViewById(R.id.ptz_contrast);
		ptzResolutoin = (ImageButton) findViewById(R.id.ptz_resolution);
		preset = (ImageButton) findViewById(R.id.preset);

		lightBtn = (ImageButton) findViewById(R.id.light);
		sireBtn = (ImageButton) findViewById(R.id.sire);

		ptztalk.setOnClickListener(this);
		ptzAudio.setOnClickListener(this);
		ptzTake_photos.setOnClickListener(this);
		ptzTake_vodeo.setOnClickListener(this);
		ptzBrightness.setOnClickListener(this);
		ptzContrast.setOnClickListener(this);
		ptzResolutoin.setOnClickListener(this);
		ptzDefaultSet.setOnClickListener(this);
		preset.setOnClickListener(this);
		lightBtn.setOnClickListener(this);
		sireBtn.setOnClickListener(this);

		Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.top_bg);
		drawable = new BitmapDrawable(bitmap);
		drawable.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		drawable.setDither(true);

		bottomView.setBackgroundDrawable(drawable);
	}

	private boolean isDown = false;
	private boolean isSecondDown = false;
	private float x1 = 0;
	private float x2 = 0;
	private float y1 = 0;
	private float y2 = 0;

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (!isDown) {
			x1 = event.getX();
			y1 = event.getY();
			isDown = true;
		}
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				savedMatrix.set(matrix);
				start.set(event.getX(), event.getY());
				mode = DRAG;
				originalScale = getScale();
				break;
			case MotionEvent.ACTION_POINTER_UP:
				break;
			case MotionEvent.ACTION_UP:
				if (Math.abs((x1 - x2)) < 25 && Math.abs((y1 - y2)) < 25) {

					if (resolutionPopWindow != null
							&& resolutionPopWindow.isShowing()) {
						resolutionPopWindow.dismiss();
					}

					if (mPopupWindowProgress != null
							&& mPopupWindowProgress.isShowing()) {
						mPopupWindowProgress.dismiss();
					}
					if (!isSecondDown) {
						if (!bProgress) {
							showBottom();
						}
					}
					isSecondDown = false;
				}
				x1 = 0;
				x2 = 0;
				y1 = 0;
				y2 = 0;
				isDown = false;
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				isSecondDown = true;
				break;
		}
		return gt.onTouchEvent(event);
	}

	private static final int NONE = 0;
	private static final int DRAG = 1;
	private static final int ZOOM = 2;

	private int mode = NONE;
	private float oldDist;
	private Matrix matrix = new Matrix();
	private Matrix savedMatrix = new Matrix();
	private PointF start = new PointF();
	float originalScale;
	protected Matrix mBaseMatrix = new Matrix();
	protected Matrix mSuppMatrix = new Matrix();
	private Matrix mDisplayMatrix = new Matrix();
	private final float[] mMatrixValues = new float[9];

	protected float getScale(Matrix matrix) {
		return getValue(matrix, Matrix.MSCALE_X);
	}

	protected float getScale() {
		return getScale(mSuppMatrix);
	}

	protected float getValue(Matrix matrix, int whichValue) {
		matrix.getValues(mMatrixValues);
		return mMatrixValues[whichValue];
	}

	@Override
	public boolean onDown(MotionEvent e) {
		Log.d("tag", "onDown");
		return false;
	}

	private final int MINLEN = 80;//滑动的最小间距
	private Animation showTopAnim;
	private Animation dismissTopAnim;
	private boolean isPTZPrompt;

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
						   float velocityY) {
		float x1 = e1.getX();
		float x2 = e2.getX();
		float y1 = e1.getY();
		float y2 = e2.getY();

		float xx = x1 > x2 ? x1 - x2 : x2 - x1;
		float yy = y1 > y2 ? y1 - y2 : y2 - y1;

		if (xx > yy) {
			if ((x1 > x2) && (xx > MINLEN)) {// right
				if (!isControlDevice)
					new ControlDeviceTask(ContentCommon.CMD_PTZ_RIGHT).execute();

			} else if ((x1 < x2) && (xx > MINLEN)) {// left
				if (!isControlDevice)
					new ControlDeviceTask(ContentCommon.CMD_PTZ_LEFT).execute();
			}

		} else {
			if ((y1 > y2) && (yy > MINLEN)) {// down
				if (!isControlDevice)
					new ControlDeviceTask(ContentCommon.CMD_PTZ_DOWN).execute();
			} else if ((y1 < y2) && (yy > MINLEN)) {// up
				if (!isControlDevice)
					new ControlDeviceTask(ContentCommon.CMD_PTZ_UP).execute();
			}

		}
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
							float distanceY) {
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}

	/**下方按钮的按键响应*/
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.ptz_talk://对讲
				goMicroPhone();
				break;
			case R.id.ptz_take_videos://录像
				goTakeVideo();
				break;
			case R.id.ptz_take_photos://拍照
				dismissBrightAndContrastProgress();
				if (existSdcard()) {// 判断sd卡是否存在
					//takePicture(mBmp);
					isTakepic = true;
				} else {
					showToast(R.string.ptz_takepic_save_fail);
				}
				break;
			case R.id.ptz_audio:
				goAudio();
				break;
			case R.id.ptz_brightness:
				if (mPopupWindowProgress != null
						&& mPopupWindowProgress.isShowing()) {
					mPopupWindowProgress.dismiss();
					mPopupWindowProgress = null;
				}
				setBrightOrContrast(BRIGHT);
				break;
			case R.id.ptz_contrast:
				if (mPopupWindowProgress != null
						&& mPopupWindowProgress.isShowing()) {
					mPopupWindowProgress.dismiss();
					mPopupWindowProgress = null;
				}
				setBrightOrContrast(CONTRAST);
				break;
			case R.id.ptz_resolution:
				showResolutionPopWindow();
				break;
			case R.id.preset://预置位设置
				presetBitWindow();
				break;
			case R.id.ptz_resolution_jpeg_qvga:
				dismissBrightAndContrastProgress();
				resolutionPopWindow.dismiss();
				nResolution = 1;
				setResolution(nResolution);
				Log.d("tag", "jpeg resolution:" + nResolution + " qvga");
				break;
			case R.id.ptz_resolution_jpeg_vga:
				dismissBrightAndContrastProgress();
				resolutionPopWindow.dismiss();
				nResolution = 0;
				setResolution(nResolution);
				Log.d("tag", "jpeg resolution:" + nResolution + " vga");
				break;
			case R.id.ptz_resolution_h264_qvga:
				dismissBrightAndContrastProgress();
				resolutionPopWindow.dismiss();
				ismax = false;
				ismiddle = false;
				ishigh = false;
				isp720 = false;
				isqvga1 = true;
				isvga1 = false;
				addReslution(stqvga1, isqvga1);
				nResolution = 5;
				setResolution(nResolution);
				break;
			case R.id.ptz_resolution_h264_vga:
				dismissBrightAndContrastProgress();
				resolutionPopWindow.dismiss();
				ismax = false;
				ismiddle = false;
				ishigh = false;
				isp720 = false;
				isqvga1 = false;
				isvga1 = true;
				addReslution(stvga1, isvga1);
				nResolution = 4;
				setResolution(nResolution);

				break;
			case R.id.ptz_resolution_h264_720p:
				dismissBrightAndContrastProgress();
				resolutionPopWindow.dismiss();
				ismax = false;
				ismiddle = false;
				ishigh = false;
				isp720 = true;
				isqvga1 = false;
				isvga1 = false;
				addReslution(stp720, isp720);
				nResolution = 3;
				setResolution(nResolution);
				break;
			case R.id.ptz_resolution_h264_middle:
				dismissBrightAndContrastProgress();
				resolutionPopWindow.dismiss();
				ismax = false;
				ismiddle = true;
				ishigh = false;
				isp720 = false;
				isqvga1 = false;
				isvga1 = false;
				addReslution(stmiddle, ismiddle);
				nResolution = 2;
				setResolution(nResolution);
				break;
			case R.id.ptz_resolution_h264_high:
				dismissBrightAndContrastProgress();
				resolutionPopWindow.dismiss();
				ismax = false;
				ismiddle = false;
				ishigh = true;
				isp720 = false;
				isqvga1 = false;
				isvga1 = false;
				addReslution(sthigh, ishigh);
				nResolution = 1;
				setResolution(nResolution);
				break;
			case R.id.ptz_resolution_h264_max:
				dismissBrightAndContrastProgress();
				resolutionPopWindow.dismiss();
				ismax = true;
				ismiddle = false;
				ishigh = false;
				isp720 = false;
				isqvga1 = false;
				isvga1 = false;
				addReslution(stmax, ismax);
				nResolution = 0;
				setResolution(nResolution);
				break;

			case R.id.ptz_default_set:
				dismissBrightAndContrastProgress();
				defaultVideoParams();
				break;

			case R.id.light:
				if (isOpen) {
					setLightState(strDID, SystemValue.devicePass, false);
					lightBtn.setBackground(getResources().getDrawable(R.drawable.camera_light_btn_off));

				} else {
					setLightState(strDID, SystemValue.devicePass, true);
					lightBtn.setBackground(getResources().getDrawable(R.drawable.camera_light_btn_on));
				}
				isOpen = !isOpen;
				break;
			case R.id.sire:
				if (isOpenSire) {
					setSireState(strDID, SystemValue.devicePass, false);
					sireBtn.setBackground(getResources().getDrawable(R.drawable.camera_siren_btn_off));

				} else {
					setSireState(strDID, SystemValue.devicePass, true);
					sireBtn.setBackground(getResources().getDrawable(R.drawable.camera_siren_btn_on));
				}
				isOpenSire = !isOpenSire;
				break;
		}
	}

	boolean isOpen = false;
	boolean isOpenSire = false;

	public static void setLightState(String did, String pwd, boolean isOpen) {
		NativeCaller.TransferMessage(did, "trans_cmd_string.cgi?cmd=2109&command=0&light=" + (isOpen ? 1 : 0) + "&loginuse=admin&loginpas=" + pwd + "&user=admin&pwd=" + pwd, 1);
	}

	public static void setSireState(String did, String pwd, boolean isOpen) {
		NativeCaller.TransferMessage(did, "trans_cmd_string.cgi?cmd=2109&command=0&siren=" + (isOpen ? 1 : 0) + "&loginuse=admin&loginpas=" + pwd + "&user=admin&pwd=" + pwd, 1);
	}

	private void dismissBrightAndContrastProgress() {
		if (mPopupWindowProgress != null && mPopupWindowProgress.isShowing()) {
			mPopupWindowProgress.dismiss();
			mPopupWindowProgress = null;
		}
	}

	private void showBottom() {
		if (isUpDownPressed) {
			isUpDownPressed = false;
			bottomView.startAnimation(dismissAnim);
			bottomView.setVisibility(View.GONE);
		} else {
			isUpDownPressed = true;
			bottomView.startAnimation(showAnim);
			bottomView.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void LightSireCallBack(String did, String command, String cmd, String siren, String light) {
		if (!did.equals(strDID)) {
			return;
		}
		if (light.equals("0")) {
			isOpen = false;
			deviceParamsHandler.sendEmptyMessage(11);

		} else {
			deviceParamsHandler.sendEmptyMessage(12);
			isOpen = true;
		}
		if (siren.equals("0")) {
			isOpenSire = false;
			deviceParamsHandler.sendEmptyMessage(13);
		} else {
			isOpenSire = true;
			deviceParamsHandler.sendEmptyMessage(14);
		}
	}

	/**
	 *异步控制摄像头朝向
	 */
	private class ControlDeviceTask extends AsyncTask<Void, Void, Integer> {
		private int type;

		public ControlDeviceTask(int type) {
			this.type = type;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if (type == ContentCommon.CMD_PTZ_RIGHT) {
				control_item.setText(R.string.right);
			} else if (type == ContentCommon.CMD_PTZ_LEFT) {
				control_item.setText(R.string.left);
			} else if (type == ContentCommon.CMD_PTZ_UP) {
				control_item.setText(R.string.up);
			} else if (type == ContentCommon.CMD_PTZ_DOWN) {
				control_item.setText(R.string.down);
			}
			if (controlWindow != null && controlWindow.isShowing())
				controlWindow.dismiss();

			if (controlWindow != null && !controlWindow.isShowing())
				controlWindow.showAtLocation(playSurface, Gravity.CENTER, 0, 0);
		}

		@Override
		protected Integer doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			isControlDevice = true;
			if (type == ContentCommon.CMD_PTZ_RIGHT) {
				NativeCaller.PPPPPTZControl(strDID, ContentCommon.CMD_PTZ_RIGHT);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				NativeCaller.PPPPPTZControl(strDID, ContentCommon.CMD_PTZ_RIGHT_STOP);
			} else if (type == ContentCommon.CMD_PTZ_LEFT) {
				NativeCaller.PPPPPTZControl(strDID, ContentCommon.CMD_PTZ_LEFT);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				NativeCaller.PPPPPTZControl(strDID, ContentCommon.CMD_PTZ_LEFT_STOP);
			} else if (type == ContentCommon.CMD_PTZ_UP) {
				NativeCaller.PPPPPTZControl(strDID, ContentCommon.CMD_PTZ_UP);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				NativeCaller.PPPPPTZControl(strDID, ContentCommon.CMD_PTZ_UP_STOP);
			} else if (type == ContentCommon.CMD_PTZ_DOWN) {
				NativeCaller.PPPPPTZControl(strDID, ContentCommon.CMD_PTZ_DOWN);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				NativeCaller.PPPPPTZControl(strDID, ContentCommon.CMD_PTZ_DOWN_STOP);
			}
			return 0;
		}

		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			isControlDevice = false;
			if (controlWindow != null && controlWindow.isShowing())
				controlWindow.dismiss();
			//每次摄像头朝向改变，需要重新开启定时器，重新初始化
			stopBackgroundThread();
			reInitPosition();
			makeInitAlarm(3);
			showToast(R.string.initing);
		}

	}
	/**
	 * 关闭已经启动的定时器
	 */
	private void reInitPosition() {
		initPosition = true;
		fail2Init = false;
		timeHandler.removeMessages(INIT_MSG);
		timeHandler.removeMessages(FATIGUE_MSG);
		timeHandler.removeMessages(NOD_MSG);
		if (timer4Init != null) {
			timer4Init.cancel();
			timer4Init = null;
		}
		if (timer4Fatigue != null) {
			timer4Fatigue.cancel();
			timer4Fatigue = null;
		}
		if (timer4Nod != null) {
			timer4Nod.cancel();
			timer4Nod = null;
		}
		initParams();
		handleTimeMsg();
	}

	/*
	 * 上下左右提示框
	 */
	private void initControlDailog() {
		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(R.layout.control_device_view, null);
		control_item = (TextView) view.findViewById(R.id.textView1_play);
		controlWindow = new PopupWindow(view, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		controlWindow.setBackgroundDrawable(new ColorDrawable(0));
		controlWindow.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				controlWindow.dismiss();
			}
		});
		controlWindow.setTouchInterceptor(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				if (arg1.getAction() == MotionEvent.ACTION_OUTSIDE) {
					controlWindow.dismiss();
				}
				return false;
			}
		});
	}

	/*
	 * 16个预置位设置面板
	 */
	private void presetBitWindow() {
		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(R.layout.preset_view, null);
		initViewPager(view);
		presetBitWindow = new PopupWindow(view, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		presetBitWindow.setAnimationStyle(R.style.AnimationPreview);
		presetBitWindow.setFocusable(true);
		presetBitWindow.setOutsideTouchable(true);
		presetBitWindow.setBackgroundDrawable(new ColorDrawable(0));
		presetBitWindow.showAtLocation(playSurface, Gravity.CENTER, 0, 0);

	}

	private void initViewPager(View view) {
		final TextView left = (TextView) view.findViewById(R.id.text_pre_left);
		final TextView rigth = (TextView) view.findViewById(R.id.text_pre_right);
		left.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				left.setTextColor(Color.BLUE);
				rigth.setTextColor(0xffffffff);
				prePager.setCurrentItem(0);
			}
		});
		rigth.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				rigth.setTextColor(Color.BLUE);
				left.setTextColor(0xffffffff);
				prePager.setCurrentItem(1);
			}
		});

		prePager = (ViewPager) view.findViewById(R.id.vPager);
		listViews = new ArrayList<View>();
		LayoutInflater mInflater = getLayoutInflater();
		View view1 = mInflater.inflate(R.layout.popuppreset, null);
		View view2 = mInflater.inflate(R.layout.popuppreset, null);
		btnLeft[0] = (Button) view1.findViewById(R.id.pre1);
		btnRigth[0] = (Button) view2.findViewById(R.id.pre1);
		btnLeft[1] = (Button) view1.findViewById(R.id.pre2);
		btnRigth[1] = (Button) view2.findViewById(R.id.pre2);
		btnLeft[2] = (Button) view1.findViewById(R.id.pre3);
		btnRigth[2] = (Button) view2.findViewById(R.id.pre3);
		btnLeft[3] = (Button) view1.findViewById(R.id.pre4);
		btnRigth[3] = (Button) view2.findViewById(R.id.pre4);
		btnLeft[4] = (Button) view1.findViewById(R.id.pre5);
		btnRigth[4] = (Button) view2.findViewById(R.id.pre5);
		btnLeft[5] = (Button) view1.findViewById(R.id.pre6);
		btnRigth[5] = (Button) view2.findViewById(R.id.pre6);
		btnLeft[6] = (Button) view1.findViewById(R.id.pre7);
		btnRigth[6] = (Button) view2.findViewById(R.id.pre7);
		btnLeft[7] = (Button) view1.findViewById(R.id.pre8);
		btnRigth[7] = (Button) view2.findViewById(R.id.pre8);
		btnLeft[8] = (Button) view1.findViewById(R.id.pre9);
		btnRigth[8] = (Button) view2.findViewById(R.id.pre9);
		btnLeft[9] = (Button) view1.findViewById(R.id.pre10);
		btnRigth[9] = (Button) view2.findViewById(R.id.pre10);
		btnLeft[10] = (Button) view1.findViewById(R.id.pre11);
		btnRigth[10] = (Button) view2.findViewById(R.id.pre11);
		btnLeft[11] = (Button) view1.findViewById(R.id.pre12);
		btnRigth[11] = (Button) view2.findViewById(R.id.pre12);
		btnLeft[12] = (Button) view1.findViewById(R.id.pre13);
		btnRigth[12] = (Button) view2.findViewById(R.id.pre13);
		btnLeft[13] = (Button) view1.findViewById(R.id.pre14);
		btnRigth[13] = (Button) view2.findViewById(R.id.pre14);
		btnLeft[14] = (Button) view1.findViewById(R.id.pre15);
		btnRigth[14] = (Button) view2.findViewById(R.id.pre15);
		btnLeft[15] = (Button) view1.findViewById(R.id.pre16);
		btnRigth[15] = (Button) view2.findViewById(R.id.pre16);
		listViews.add(view1);
		listViews.add(view2);
		prePager.setAdapter(new ViewPagerAdapter(listViews));
		prePager.setCurrentItem(0);
		prePager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int arg0) {
				if (arg0 == 0) {
					left.setTextColor(Color.BLUE);
					rigth.setTextColor(0xffffffff);
				} else {
					rigth.setTextColor(Color.BLUE);
					left.setTextColor(0xffffffff);
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		PresetListener listener = new PresetListener();
		for (int i = 0; i < 16; i++) {
			btnLeft[i].setOnClickListener(listener);
			btnRigth[i].setOnClickListener(listener);
		}
	}

	//预位置设置监听
	private class PresetListener implements OnClickListener {
		@Override
		public void onClick(View arg0) {
			int id = arg0.getId();
			presetBitWindow.dismiss();
			int currIndex = prePager.getCurrentItem();
			switch (id) {
				case R.id.pre1:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 31);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 30);
					}
					break;

				case R.id.pre2:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 33);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 32);
					}
					break;

				case R.id.pre3:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 35);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 34);
					}

				case R.id.pre4:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 37);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 36);
					}
					break;

				case R.id.pre5:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 39);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 38);
					}
					break;

				case R.id.pre6:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 41);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 40);
					}
					break;

				case R.id.pre7:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 43);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 42);
					}
					break;

				case R.id.pre8:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 45);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 44);
					}
					break;

				case R.id.pre9:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 47);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 46);
					}
					break;

				case R.id.pre10:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 49);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 48);
					}
					break;

				case R.id.pre11:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 51);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 50);
					}
					break;

				case R.id.pre12:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 53);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 52);
					}
					break;

				case R.id.pre13:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 55);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 54);
					}
					break;

				case R.id.pre14:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 57);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 56);
					}
					break;

				case R.id.pre15:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 59);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 58);
					}
					break;

				case R.id.pre16:
					if (currIndex == 0) {
						NativeCaller.PPPPPTZControl(strDID, 61);
					} else {
						NativeCaller.PPPPPTZControl(strDID, 60);
					}
					break;
			}
		}
	}

	//判断sd卡是否存在
	private boolean existSdcard() {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			return true;
		} else {
			return false;
		}
	}

	// 拍照
	private void takePicture(final Bitmap bmp) {
		if (!isPictSave) {
			isPictSave = true;
			new Thread() {
				public void run() {

					savePicToSDcard(bmp);
				}
			}.start();
		} else {
			return;
		}
	}

	/*
	 * 保存到本地
	 * 注意：此处可以做本地数据库sqlit 保存照片，以便于到本地照片观看界面从SQLite取出照片
	 */
	private synchronized void savePicToSDcard(final Bitmap bmp) {
		String strDate = getStrDate();
		//String date = strDate.substring(0, 10);
		FileOutputStream fos = null;
		try {
			File div = new File(Environment.getExternalStorageDirectory(),
					"ipcameradetect/takepic");
			if (!div.exists()) {
				div.mkdirs();
			}
			++picNum;
			File file = new File(div, strDate + "_" + strDID + "_" + picNum+ ".jpg");
			fos = new FileOutputStream(file);
			if (bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos)) {
				fos.flush();
				Log.d("tag", "takepicture success");
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						showToast(R.string.ptz_takepic_ok);
					}
				});
			}
		} catch (Exception e) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					showToast(R.string.ptz_takepic_fail);
				}
			});
			Log.d("tag", "exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			isPictSave = false;
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				fos = null;
			}
		}
	}

	//时间格式
	private String getStrDate() {
		Date d = new Date();
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd_HH_mm");
		String strDate = f.format(d);
		return strDate;
	}

	/*
	 * 录像
	 */
	private void goTakeVideo() {
		dismissBrightAndContrastProgress();
		if (isTakeVideo) {
			showToast(R.string.ptz_takevideo_end);
			Log.d("tag", "停止录像 stop");

			String strRecord = "";
			NativeCaller.RecordLocal(strDID, strRecord, 0);
			isTakeVideo = false;
			ptzTake_vodeo.setImageResource(R.drawable.ptz_takevideo);
		} else {
			isTakeVideo = true;
			showToast(R.string.ptz_takevideo_begin);
			Log.d("tag", "开始录像  start");
			videotime = (new Date()).getTime();
			ptzTake_vodeo.setImageResource(R.drawable.ptz_takevideo_pressed);
			File div = new File(Environment.getExternalStorageDirectory(),
					"ipcameradetect/takepic");
			if (!div.exists()) {
				div.mkdirs();
			}
			File file = new File(div, strDID + "_" + getStrDate() + ".mp4");
			NativeCaller.RecordLocal(strDID, file.getAbsolutePath(), 1);
			NativeCaller.TransferMessage(strDID, "camera_control.cgi?param=26&value=" + 1 + "&loginuse=admin&loginpas=" + SystemValue.devicePass + "&user=admin&pwd=" + SystemValue.devicePass, 1);
			//strRecord  录像文件路径，可以根据实际需求自行定义
		}
	}


	private void stopTakevideo() {
		if (isTakeVideo) {
			showToast(R.string.ptz_takevideo_end);
			Log.d("tag", "停止录像");
			isTakeVideo = false;
			// cameratakevideo.stopRecordVideo(strDID);
		}
	}

	//讲话
	private void StartTalk() {
		if (customAudioRecorder != null) {
			Log.i("info", "startTalk");
			customAudioRecorder.StartRecord();
			NativeCaller.PPPPStartTalk(strDID);
		}
	}

	//停止讲话
	private void StopTalk() {
		if (customAudioRecorder != null) {
			Log.i("info", "stopTalk");
			customAudioRecorder.StopRecord();
			NativeCaller.PPPPStopTalk(strDID);
		}
	}

	//监听
	private void StartAudio() {
		synchronized (this) {
			AudioBuffer.ClearAll();
			audioPlayer.AudioPlayStart();
			NativeCaller.PPPPStartAudio(strDID);
		}
	}

	//停止监听
	private void StopAudio() {
		synchronized (this) {
			audioPlayer.AudioPlayStop();
			AudioBuffer.ClearAll();
			NativeCaller.PPPPStopAudio(strDID);
		}
	}

	/*
	 * 监听
	 */
	private void goAudio() {
		dismissBrightAndContrastProgress();
		if (!isMcriophone) {
			if (bAudioStart) {
				Log.d("info", "没有声音");
				isTalking = false;
				bAudioStart = false;
				ptzAudio.setImageResource(R.drawable.ptz_audio_off);
				StopAudio();
			} else {
				Log.d("info", "有声");
				isTalking = true;
				bAudioStart = true;
				ptzAudio.setImageResource(R.drawable.ptz_audio_on);
				StartAudio();
			}

		} else {
			isMcriophone = false;
			bAudioRecordStart = false;
			ptztalk.setImageResource(R.drawable.ptz_microphone_off);
			StopTalk();
			isTalking = true;
			bAudioStart = true;
			ptzAudio.setImageResource(R.drawable.ptz_audio_on);
			StartAudio();
		}

	}

	/*
	 * 对讲
	 */
	private void goMicroPhone() {
		dismissBrightAndContrastProgress();
		if (!isTalking) {
			if (bAudioRecordStart) {
				Log.d("tag", "停止说话");
				isMcriophone = false;
				bAudioRecordStart = false;
				ptztalk.setImageResource(R.drawable.ptz_microphone_off);
				StopTalk();
			} else {
				Log.d("info", "开始说话");
				isMcriophone = true;
				bAudioRecordStart = true;
				ptztalk.setImageResource(R.drawable.ptz_microphone_on);
				StartTalk();
			}
		} else {
			isTalking = false;
			bAudioStart = false;
			ptzAudio.setImageResource(R.drawable.ptz_audio_off);
			StopAudio();
			isMcriophone = true;
			bAudioRecordStart = true;
			ptztalk.setImageResource(R.drawable.ptz_microphone_on);
			StartTalk();
		}

	}

	/*
	 * 分辨率设置
	 */
	private void showResolutionPopWindow() {

		if (resolutionPopWindow != null && resolutionPopWindow.isShowing()) {
			return;
		}
		if (nStreamCodecType == ContentCommon.PPPP_STREAM_TYPE_JPEG) {
			// jpeg
			LinearLayout layout = (LinearLayout) LayoutInflater.from(this)
					.inflate(R.layout.ptz_resolution_jpeg, null);
			TextView qvga = (TextView) layout
					.findViewById(R.id.ptz_resolution_jpeg_qvga);
			TextView vga = (TextView) layout
					.findViewById(R.id.ptz_resolution_jpeg_vga);
			if (reslutionlist.size() != 0) {
				getReslution();
			}
			if (isvga) {
				vga.setTextColor(Color.RED);
			}
			if (isqvga) {
				qvga.setTextColor(Color.RED);
			}
			qvga.setOnClickListener(this);
			vga.setOnClickListener(this);
			resolutionPopWindow = new PopupWindow(layout, 100,
					WindowManager.LayoutParams.WRAP_CONTENT);
			int x_begin = getWindowManager().getDefaultDisplay().getWidth() / 6;
			int y_begin = ptzResolutoin.getTop();
			resolutionPopWindow.showAtLocation(findViewById(R.id.play),
					Gravity.BOTTOM | Gravity.RIGHT, x_begin, y_begin);

		} else {
			// h264
			LinearLayout layout = (LinearLayout) LayoutInflater.from(this)
					.inflate(R.layout.ptz_resolution_h264, null);
			TextView qvga1 = (TextView) layout
					.findViewById(R.id.ptz_resolution_h264_qvga);
			TextView vga1 = (TextView) layout
					.findViewById(R.id.ptz_resolution_h264_vga);
			TextView p720 = (TextView) layout
					.findViewById(R.id.ptz_resolution_h264_720p);
			TextView middle = (TextView) layout
					.findViewById(R.id.ptz_resolution_h264_middle);
			TextView high = (TextView) layout
					.findViewById(R.id.ptz_resolution_h264_high);
			TextView max = (TextView) layout
					.findViewById(R.id.ptz_resolution_h264_max);

			if (reslutionlist.size() != 0) {
				getReslution();
			}
			if (ismax) {
				max.setTextColor(Color.RED);
			}
			if (ishigh) {
				high.setTextColor(Color.RED);
			}
			if (ismiddle) {
				middle.setTextColor(Color.RED);
			}
			if (isqvga1) {
				qvga1.setTextColor(Color.RED);
			}
			if (isvga1) {
				vga1.setTextColor(Color.RED);
			}
			if (isp720) {
				p720.setTextColor(Color.RED);
			}
			high.setOnClickListener(this);
			middle.setOnClickListener(this);
			max.setOnClickListener(this);
			qvga1.setOnClickListener(this);
			vga1.setOnClickListener(this);
			p720.setOnClickListener(this);
			resolutionPopWindow = new PopupWindow(layout, 100,
					WindowManager.LayoutParams.WRAP_CONTENT);
			Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
					.getDefaultDisplay();
			int oreation = display.getOrientation();
			int x_begin = getWindowManager().getDefaultDisplay().getWidth() / 6;
			int y_begin = ptzResolutoin.getTop();
			resolutionPopWindow.showAtLocation(findViewById(R.id.play),
					Gravity.BOTTOM | Gravity.RIGHT, x_begin, y_begin + 60);

		}

	}

	/**
	 * 获取reslution
	 */
	public static Map<String, Map<Object, Object>> reslutionlist = new HashMap<String, Map<Object, Object>>();

	/**
	 * 增加reslution 在之前的视频流回调的Handler处调用
	 */
	private void addReslution(String mess, boolean isfast) {
		if (reslutionlist.size() != 0) {
			if (reslutionlist.containsKey(strDID)) {
				reslutionlist.remove(strDID);
			}
		}
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put(mess, isfast);
		reslutionlist.put(strDID, map);
	}

	private void getReslution() {
		if (reslutionlist.containsKey(strDID)) {
			Map<Object, Object> map = reslutionlist.get(strDID);
			if (map.containsKey("qvga")) {
				isqvga = true;
			} else if (map.containsKey("vga")) {
				isvga = true;
			} else if (map.containsKey("qvga1")) {
				isqvga1 = true;
			} else if (map.containsKey("vga1")) {
				isvga1 = true;
			} else if (map.containsKey("p720")) {
				isp720 = true;
			} else if (map.containsKey("high")) {
				ishigh = true;
			} else if (map.containsKey("middle")) {
				ismiddle = true;
			} else if (map.containsKey("max")) {
				ismax = true;
			}
		}
	}

	/**
	 * @param type
	 * 亮度饱和对比度
	 */
	private void setBrightOrContrast(final int type) {

		if (!bInitCameraParam) {
			return;
		}
		int width = getWindowManager().getDefaultDisplay().getWidth();
		LinearLayout layout = (LinearLayout) LayoutInflater.from(this).inflate(
				R.layout.brightprogress, null);
		SeekBar seekBar = (SeekBar) layout.findViewById(R.id.brightseekBar1);
		seekBar.setMax(255);
		switch (type) {
			case BRIGHT:
				seekBar.setProgress(nBrightness);
				break;
			case CONTRAST:
				seekBar.setProgress(nContrast);
				break;
			default:
				break;
		}
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				int progress = seekBar.getProgress();
				switch (type) {
					case BRIGHT:// 亮度
						nBrightness = progress;
						NativeCaller.PPPPCameraControl(strDID, BRIGHT, nBrightness);
						break;
					case CONTRAST:// 对比度
						nContrast = progress;
						NativeCaller.PPPPCameraControl(strDID, CONTRAST, nContrast);
						break;
					default:
						break;
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {

			}

			@Override
			public void onProgressChanged(SeekBar arg0, int progress,
										  boolean arg2) {

			}
		});
		mPopupWindowProgress = new PopupWindow(layout, width / 2, 180);
		mPopupWindowProgress.showAtLocation(findViewById(R.id.play),
				Gravity.TOP, 0, 0);

	}

	@Override
	protected synchronized void onDestroy() {
		NativeCaller.StopPPPPLivestream(strDID);//提示摄像头停止传输视频流
		StopAudio();//停止播放音频
		StopTalk();//停止对讲
		stopTakevideo();//停止录像
		releaseAll();//释放定时器和tenginekit面部检测模型
		classifier.close();//关闭tensorflow的分类器模型
		super.onDestroy();
	}

	/**
	 * 释放定时器和tenginekit面部检测模型
	 */
	public void releaseAll() {
		timeHandler.removeMessages(INIT_MSG);
		timeHandler.removeMessages(FATIGUE_MSG);
		timeHandler.removeMessages(NOD_MSG);
		if (alarmPlayer != null)
			alarmPlayer.release();
		if (timer4Init != null) {
			timer4Init.cancel();
			timer4Init = null;
		}
		if (timer4Fatigue != null) {
			timer4Fatigue.cancel();
			timer4Fatigue = null;
		}
		if (timer4Nod != null) {
			timer4Nod.cancel();
			timer4Nod = null;
		}
		if (timeHandler != null) {
			timeHandler = null;
		}
		KitCore.release();
	}

	/***
	 * BridgeService callback 相机参数回调
	 * @Param did：摄像机id
	 * @Param brightness：亮度
	 * @Param constrat：对比度
	 * @Param flip：是否开启了警报和灯（该软件系统对应摄像头无法使用）
	 * **/
	@Override
	public void callBackCameraParamNotify(String did, int resolution,
										  int brightness, int contrast, int hue, int saturation, int flip, int mode) {
		//Log.e("设备返回的参数", resolution+","+brightness+","+contrast+","+hue+","+saturation+","+flip+","+mode);
		nBrightness = brightness;
		nContrast = contrast;
		nResolution = resolution;
		bInitCameraParam = true;
		deviceParamsHandler.sendEmptyMessage(flip);
	}

	/**
	 * 视频流回调，并开启人脸检测
	 * @param videobuf 视频流数据
	 * @param h264Data 是否为h264格式的数据
	 * @param len 数据长度
	 * @param width 视频流宽度
	 * @param height 视频流高度
	 */
	@Override
	public void callBackVideoData(final byte[] videobuf, int h264Data, int len, int width, int height) {
		if (!bDisplayFinished)
			return;
		//mine
		previewWidth = width;
		previewHeight = height;
		try {
			if (mNV21Bytes == null) {
				Init();//初始化tengineKit
			}
		} catch (final Exception e) {
			Log.e(LOG_TAG, "callBackVideoData: " + e);
			return;
		}
		imageConverter = new Runnable() {
			@Override
			public void run() {
				mNV21Bytes = videobuf;
			}
		};
		processImage();//调用检测函数
		//mine
		bDisplayFinished = false;
		videodata = videobuf;
		videoDataLen = len;
		Message msg = new Message();
		if (h264Data == 1) { // H264
			nVideoWidths = width;
			nVideoHeights = height;
			if (isTakepic) {
				isTakepic = false;
				byte[] rgb = new byte[width * height * 2];
				NativeCaller.YUV4202RGB565(videobuf, rgb, width, height);
				ByteBuffer buffer = ByteBuffer.wrap(rgb);
				mBmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
				mBmp.copyPixelsFromBuffer(buffer);
				takePicture(mBmp);
			}
			isH264 = true;
			msg.what = 1;
		} else { // MJPEG
			isJpeg = true;
			msg.what = 2;
		}
		mHandler.sendMessage(msg);
		//录像数据
		if (isTakeVideo) {
			Date date = new Date();
			long times = date.getTime();
			int tspan = (int) (times - videotime);
			Log.d("tag", "play  tspan:" + tspan);
			videotime = times;
			if (videoRecorder != null) {
				if (isJpeg) {
					videoRecorder.VideoRecordData(2, videobuf, width, height, tspan);
				}
			}
		}
	}

	//mine
	//得到最新的视频流bytes
	protected void getCameraBytes() {
		if (imageConverter != null) {
			imageConverter.run();
		}
	}

	//设置输出检测的屏幕大小
	public void setScreenSize(int previewWidth, int previewHeight) {
		final double width = getResources().getDisplayMetrics().widthPixels;
		final double height = getResources().getDisplayMetrics().heightPixels;

		if (width < height * previewWidth / previewHeight) {
			ScreenWidth = width;
			ScreenHeight = width * previewHeight / previewWidth;

		} else {
			ScreenWidth = height * previewWidth / previewHeight;
			ScreenHeight = height;
		}
	}

	//初始化人脸检测功能
	public void Init() {
		setScreenSize(previewWidth, previewHeight);
		mNV21Bytes = new byte[previewHeight * previewWidth];
		KitCore.init(
				PlayActivity.this,
				AndroidConfig
						.create()
						.setNormalMode()
						.setDefaultFunc().openFunc(AndroidConfig.Func.FaceMesh).openFunc(AndroidConfig.Func.Iris)
						.setDefaultInputImageFormat()
						.setInputImageSize(previewWidth, previewHeight)
						.setOutputImageSize((int) ScreenWidth, (int) ScreenHeight)
		);
	}

	/**
	 * 获取人脸特征数据并获取疲劳检测的有关数值
	 */
	protected void processImage() {
		if (mNV21Bytes == null)
			return;
		//获得回调函数
		getCameraBytes();
		//设置镜头的旋转角，手机上设置比较复杂，需要考虑翻转，这里只需要普通设置就行
		KitCore.Camera.setRotation(0, false, (int) PlayActivity.ScreenWidth, (int) PlayActivity.ScreenHeight);
		//检测人脸
		Face.FaceDetect faceDetect = Face.detect(mNV21Bytes);
		//人脸检测信息
		List<FaceDetectInfo> faceDetectInfos = new ArrayList<>();
		//人脸2d关键点信息
		List<FaceLandmarkInfo> faceLandmarkInfos = new ArrayList<>();
		//人脸3d关键点信息
		List<FaceLandmark3dInfo> faceLandmark3dInfos = new ArrayList<>();
		//人脸瞳孔信息
		List<FaceIrisInfo> faceIrisInfo = new ArrayList<>();
		if (faceDetect.getFaceCount() > 0) {//当检测到人脸时
			notDetectFrame = 0;
			faceDetectInfos = faceDetect.getDetectInfos();
			faceLandmark3dInfos = faceDetect.landmark3d();
			faceIrisInfo = faceDetect.iris3d();
			faceLandmarkInfos = faceDetect.landmark2d();
			FaceLandmarkInfo faceAttr = faceLandmarkInfos.get(0);
			if (initPosition) {
				initHeadPosition(faceAttr);
				initParams();
			} else {
				//1分种内的帧数
				totalFrameNumber = totalFrameNumber + 1;
				judgeEyesClose(faceAttr);
				judgeYawn(faceAttr);
				judgeNod(faceAttr);
				judgeAttention(faceIrisInfo.get(0), faceAttr);
			}
		} else {
			if (notDetectFrame >= 50) {
				notDetectFrame = 0;
				if (initPosition) {//当处于初始姿态情况下
					fail2Init = true;
				}
				makenotDetectFrameAlarm();
			} else {
				notDetectFrame = notDetectFrame + 1;
			}
		}
		//绘制人脸矩形框图
		if (faceDetectInfos != null && faceDetectInfos.size() > 0) {
			Rect rect = new Rect();
			//返回人脸识别的人脸框
			rect = faceDetectInfos.get(0).asRect();
			//重新设置参数
			mFaceRect.setParams(rect, playSurface.getTop(), textosd.getMeasuredHeight());
			//在主线程里更新视图
			mFaceRect.invalidate();
		}
	}
	//初始化人脸检测的有关参数
	private void initParams() {
		totalFrameNumber = 0;
		twiceTimes = 2;
		fatigueDegree = 0;
		perclosDegree = 0;
		avgBlinkTime = 0;
		eyesCloseNumber = 0;
		blinkNumber = 0;
		isEyesClose = false;
		yawnNumber = 0;
		isSmallYawn = false;
		isBigYawn = false;
		yawnFrame1 = 0;
		yawnFrame2 = 0;
		maxYawnFrame1 = 0;
		maxYawnFrame2 = 0;
		headNodFrame = 0;
		headNormalFrame = 0;
		loseNoticeFrame = 0;
	}

	/**
	 * 初始化头部姿态，记录这段时间内的信息和，后求平均值
	 * @param faceAttr：人脸信息
	 */
	private void initHeadPosition(FaceLandmarkInfo faceAttr) {
		totalPitch = totalPitch + faceAttr.pitch;
		totalRoll = totalRoll + faceAttr.roll;
		totalYaw = totalYaw + faceAttr.yaw;
		initFrame = initFrame + 1;
	}

	/**
	 * 获取闭眼的帧数和眨眼的次数
	 * @param faceAttr：人脸信息
	 */
	private void judgeEyesClose(FaceLandmarkInfo faceAttr) {
		if (initPosition) return;
		if (faceAttr.leftEyeClose >= eyesCloseThreshold
				&& faceAttr.rightEyeClose >= eyesCloseThreshold) {
			eyesCloseNumber = eyesCloseNumber + 1;
			isEyesClose = true;
		} else if (isEyesClose) {
			isEyesClose = false;
			blinkNumber = blinkNumber + 1;
		}
	}

	/**
	 * 获取一分钟打哈欠的个数
	 * @param faceAttr：人脸信息
	 */
	private void judgeYawn(FaceLandmarkInfo faceAttr) {
		if (initPosition) return;
		if (faceAttr.mouseOpenBig >= yawnThreshold1) {//如果张嘴程度大于阈值1
			if (isBigYawn) {//上一帧是处于张嘴程度大于阈值1的情况
				//yawnFrame1、2分别是大于目前记录的连续大于阙值1和2的帧数
				yawnFrame1 = yawnFrame1 + 1;
				yawnFrame2 = yawnFrame2 + 1;
			} else if (isSmallYawn) {//上一帧是处于张嘴程度小于阈值1，大于阙值2的情况
				yawnFrame1 = yawnFrame1 + 1;
				yawnFrame2 = yawnFrame2 + 1;
				//改变张嘴的状态
				isSmallYawn = false;
				isBigYawn = true;
			} else {//上一帧是小于阙值2的情况
				yawnFrame1 = yawnFrame1 + 1;
				yawnFrame2 = yawnFrame2 + 1;
				isBigYawn = true;
			}
		} else if (faceAttr.mouseOpenBig >= yawnThreshold2) {//如果张嘴程度大于阈值2，小于阙值1
			if (isBigYawn) {//上一帧是处于张嘴程度大于阈值1的情况
				maxYawnFrame1 = Math.max(yawnFrame1, maxYawnFrame1);//记录连续的大于阙值1的最大帧数
				yawnFrame1 = 0;
				yawnFrame2 = yawnFrame2 + 1;
				isSmallYawn = true;
				isBigYawn = false;
			} else if (isSmallYawn) {
				yawnFrame2 = yawnFrame2 + 1;
			} else {
				isSmallYawn = true;
				yawnFrame2 = yawnFrame2 + 1;
			}
		} else {//如果张嘴程度小于阙值2
			if (isBigYawn || isSmallYawn) {//如果上一帧是张开嘴的
				maxYawnFrame1 = Math.max(yawnFrame1, maxYawnFrame1);//记录连续的大于阙值1的最大帧数
				maxYawnFrame2 = Math.max(yawnFrame2, maxYawnFrame2);//记录连续的大于阙值2的最大帧数
				yawnFrame1 = 0;
				yawnFrame2 = 0;
				isBigYawn = false;
				isSmallYawn = false;
				//进行哈欠判定
				if (maxYawnFrame1 >= yawnFrameThreshold2 ||
						(maxYawnFrame1 >= yawnFrameThreshold1 && maxYawnFrame2 >= yawnFrameThreshold3)) {
					yawnNumber = yawnNumber + 1;
				}
				maxYawnFrame1 = 0;
				maxYawnFrame2 = 0;
			}
		}
	}

	/**
	 * 初始化头部姿态以及判定点头
	 * @param faceAttr：人脸信息
	 */
	private void judgeNod(FaceLandmarkInfo faceAttr) {
		if (initPosition) return;
		if (Math.abs(faceAttr.pitch - normalPitch) >= 30 || Math.abs(faceAttr.roll - normalRoll) >= 25) {
			headNodFrame = headNodFrame + 1;
		} else {
			headNormalFrame = headNormalFrame + 1;
		}
	}

	/**
	 * 根据视线判定注意力
	 * @param faceIrisInfo：瞳孔关键点信息
	 * @param faceAttr：人脸信息
	 */
	private void judgeAttention(FaceIrisInfo faceIrisInfo, FaceLandmarkInfo faceAttr) {
		if (initPosition) return;
		if (isEyesClose) {
			loseNoticeFrame = 0;
			return;
		}
		TenginekitPoint leftCenter = faceIrisInfo.leftEye.eyeIris.get(0);
		TenginekitPoint leftCorner = faceIrisInfo.leftEye.eyeLandmark.get(8);
		TenginekitPoint leftOuterCorner = faceIrisInfo.leftEye.eyeLandmark.get(0);
		float deviation = (leftCenter.X - leftOuterCorner.X) / (leftCorner.X - leftOuterCorner.X);
		if (deviation < noticeThreshold1 || deviation > noticeThreshold2 || Math.abs(faceAttr.yaw - normalYaw) > 20) {
			loseNoticeFrame = loseNoticeFrame + 1;
		} else {
			loseNoticeFrame = 0;
		}
		if (loseNoticeFrame >= 40) {
			makeNoticeAlarm();
			loseNoticeFrame = 0;
		}
	}

	//发出初始化提示警报
	public void makeInitAlarm(int flag) {
		if (isAlarming) return;
		if (alarmPlayer != null) {
			alarmPlayer.release();
		}
		switch (flag) {
			case 0:
				alarmPlayer = MediaPlayer.create(this, R.raw.init_fail);
				alarmPlayer.setOnCompletionListener(onCompletionListener);
				alarmPlayer.start();
				isAlarming = true;
				break;
			case 1:
				alarmPlayer = MediaPlayer.create(this, R.raw.init_success);
				alarmPlayer.setOnCompletionListener(onCompletionListener);
				alarmPlayer.start();
				isAlarming = true;
				break;
			case 2:
				alarmPlayer = MediaPlayer.create(this, R.raw.init_fail_notdetect);
				alarmPlayer.setOnCompletionListener(onCompletionListener);
				alarmPlayer.start();
				isAlarming = true;
				break;
			case 3:
				alarmPlayer = MediaPlayer.create(this, R.raw.reinit);
				alarmPlayer.setOnCompletionListener(onCompletionListener);
				alarmPlayer.start();
				isAlarming = true;
				break;
			default:
				break;
		}
	}
	//发出未检测到人的警报
	public void makenotDetectFrameAlarm() {
		if (isAlarming || initPosition) return;
		if (alarmPlayer != null) {
			alarmPlayer.release();
		}
		alarmPlayer = MediaPlayer.create(this, R.raw.notdetect);
		alarmPlayer.setOnCompletionListener(onCompletionListener);
		alarmPlayer.start();
		isAlarming = true;
	}

	//根据疲劳程度不同发出不同警报
	public void makeFatigueAlarm() {
		if (isAlarming || initPosition) return;
		if (fatigueDegree <= fatigueThreshold1) {
			twiceTimes = 2;
			return;
		}
		if (fatigueDegree <= fatigueThreshold2) {
			twiceTimes--;
			if (twiceTimes == 0) {
				if (alarmPlayer != null) {
					alarmPlayer.release();
				}
				alarmPlayer = MediaPlayer.create(this, R.raw.fatigue1);
				alarmPlayer.setOnCompletionListener(onCompletionListener);
				alarmPlayer.start();
				isAlarming = true;
				twiceTimes = 2;
			}
			return;
		}
		if (fatigueDegree <= fatigueThreshold3) {
			twiceTimes = 2;
			if (alarmPlayer != null) {
				alarmPlayer.release();
			}
			alarmPlayer = MediaPlayer.create(this, R.raw.fatigue2);
			alarmPlayer.setOnCompletionListener(onCompletionListener);
			alarmPlayer.start();
			isAlarming = true;
			return;
		}
		if (fatigueDegree <= fatigueThreshold4) {
			twiceTimes = 2;
			if (alarmPlayer != null) {
				alarmPlayer.release();
			}
			alarmPlayer = MediaPlayer.create(this, R.raw.fatigue3);
			alarmPlayer.setOnCompletionListener(onCompletionListener);
			alarmPlayer.start();
			isAlarming = true;
			return;
		}
		twiceTimes = 2;
		if (alarmPlayer != null) {
			alarmPlayer.release();
		}
		alarmPlayer = MediaPlayer.create(this, R.raw.fatigue4);
		alarmPlayer.setOnCompletionListener(onCompletionListener);
		alarmPlayer.start();
		isAlarming = true;
	}

	//发出头部位置不正常的警报
	public void makeNodAlarm() {
		if (isAlarming || initPosition) return;
		if (alarmPlayer != null) {
			alarmPlayer.release();
		}
		alarmPlayer = MediaPlayer.create(this, R.raw.headnod);
		alarmPlayer.setOnCompletionListener(onCompletionListener);
		alarmPlayer.start();
		isAlarming = true;
	}

	//发出注意力不集中的警报
	public void makeNoticeAlarm() {
		if (isAlarming || initPosition) return;
		if (alarmPlayer != null) {
			alarmPlayer.release();
		}
		alarmPlayer = MediaPlayer.create(this, R.raw.notice);
		alarmPlayer.setOnCompletionListener(onCompletionListener);
		alarmPlayer.start();
		isAlarming = true;
	}
	//发出使用手机的警报
	public void makePhoneAlarm() {
		if (isAlarming || initPosition) return;
		if (alarmPlayer != null) {
			alarmPlayer.release();
		}
		alarmPlayer = MediaPlayer.create(this, R.raw.phone);
		alarmPlayer.setOnCompletionListener(onCompletionListener);
		alarmPlayer.start();
		isAlarming = true;
	}
	//发出吸烟的警报
	public void makeSmokingAlarm() {
		if (isAlarming || initPosition) return;
		if (alarmPlayer != null) {
			alarmPlayer.release();
		}
		alarmPlayer = MediaPlayer.create(this, R.raw.smoke);
		alarmPlayer.setOnCompletionListener(onCompletionListener);
		alarmPlayer.start();
		isAlarming = true;
	}
	//重置疲劳检测的有关参数
	private void reSetParameter() {
		totalFrameNumber = 0;
		twiceTimes = 2;
		perclosDegree = 0;
		avgBlinkTime = 0;
		yawnNumber = 0;
		eyesCloseNumber = 0;
		blinkNumber = 0;
		fatigueDegree = 0;
	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		handleTimeMsg();//开启定时器
	}

	@Override
	public void onPause() {
		stopBackgroundThread();
		super.onPause();
	}
	//定时处理监控信息，判定疲劳和偏头行为
	public void handleTimeMsg() {
		timeHandler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				switch (msg.what) {
					case INIT_MSG:
						if (initPosition) {
							if (fail2Init) {
								totalPitch = 0;
								totalRoll = 0;
								totalYaw = 0;
								initFrame = 0;
								timeHandler.sendEmptyMessageDelayed(INIT_MSG, 15000);// 15s后发msg
								makeInitAlarm(2);
								fail2Init = false;
								break;
							}
							normalPitch = totalPitch / initFrame;
							normalRoll = totalRoll / initFrame;
							normalYaw = totalYaw / initFrame;
							if (Math.abs(normalYaw) < 2) {
								totalPitch = 0;
								totalRoll = 0;
								totalYaw = 0;
								initFrame = 0;
								initPosition = false;
								startOtherSchedule();
								makeInitAlarm(1);
								showToast(R.string.init_success);
							} else {
								totalPitch = 0;
								totalRoll = 0;
								totalYaw = 0;
								initFrame = 0;
								timeHandler.sendEmptyMessageDelayed(INIT_MSG, 15000);// 15s后发msg
								makeInitAlarm(0);
								showToast(R.string.init_fail);
							}
						}
						break;
					case FATIGUE_MSG:
						perclosDegree = (double) eyesCloseNumber / (double) totalFrameNumber;
						avgBlinkTime = (perclosDegree * 60) / blinkNumber;
						fatigueDegree = perclosDegree + 0.8 * avgBlinkTime + 0.5 * yawnNumber;
						makeFatigueAlarm();
						reSetParameter();
						break;
					case NOD_MSG:
						if (0.7 * headNodFrame > 0.3 * headNormalFrame) {
							makeNodAlarm();
						}
						headNodFrame = 0;
						headNormalFrame = 0;
						break;
					default:
						break;
				}
				return false;
			}
		});
		timeHandler.sendEmptyMessageDelayed(INIT_MSG, 15000);// 15s后发msg
	}

	//开始其他定时检测
	protected void startOtherSchedule() {
		if (timer4Init != null) {
			timer4Init.cancel();
			timer4Init = null;
		}
		timer4Fatigue = new Timer();
		timer4Fatigue.schedule(new TimerTask() {
			@Override
			public void run() {
				//  使用handler发送消息
				Message message = new Message();
				message.what = FATIGUE_MSG;
				timeHandler.sendMessage(message);
			}
		}, 60000, 60000);//每隔60秒使用handler发送一下消息

		timer4Nod = new Timer();
		timer4Nod.schedule(new TimerTask() {
			@Override
			public void run() {
				//  使用handler发送消息
				Message message = new Message();
				message.what = NOD_MSG;
				timeHandler.sendMessage(message);
			}
		}, 10000, 10000);//每隔10秒使用handler发送一下消息
		startBackgroundThread();
	}
	//mine

	/***
	 * BridgeService callback
	 *
	 * **/
	@Override
	public void callBackMessageNotify(String did, int msgType, int param) {
		/*Log.d("tag", "MessageNotify did: " + did + " msgType: " + msgType
				+ " param: " + param);*/
		if (bManualExit)
			return;

		if (msgType == ContentCommon.PPPP_MSG_TYPE_STREAM) {
			nStreamCodecType = param;
			return;
		}

		if (msgType != ContentCommon.PPPP_MSG_TYPE_PPPP_STATUS) {
			return;
		}

		if (!did.equals(strDID)) {
			return;
		}

		Message msg = new Message();
		msg.what = 1;
		msgHandler.sendMessage(msg);
	}

	/***
	 * BridgeService callback
	 *
	 * **/
	@Override
	public void callBackAudioData(byte[] pcm, int len) {
		Log.d(LOG_TAG, "AudioData: len :+ " + len);

		if (!audioPlayer.isAudioPlaying()) {
			return;
		}
		CustomBufferHead head = new CustomBufferHead();
		CustomBufferData data = new CustomBufferData();
		head.length = len;
		head.startcode = AUDIO_BUFFER_START_CODE;
		data.head = head;
		data.data = pcm;
		AudioBuffer.addData(data);
	}

	/***
	 * BridgeService callback
	 *
	 * **/
	@Override
	public void callBackH264Data(byte[] h264, int type, int size) {
		//Log.d("tag", "CallBack_H264Data" + " type:" + type + " size:" + size);
		if (isTakeVideo) {
			Date date = new Date();
			long time = date.getTime();
			int tspan = (int) (time - videotime);
			Log.d("tag", "play  tspan:" + tspan);
			videotime = time;
			if (videoRecorder != null) {
				videoRecorder.VideoRecordData(type, h264, size, 0, tspan);
			}
		}
	}

	//对讲数据
	@Override
	public void AudioRecordData(byte[] data, int len) {
		// TODO Auto-generated method stub
		if (bAudioRecordStart && len > 0) {
			NativeCaller.PPPPTalkAudioData(strDID, data, len);
		}
	}

	//定义录像接口
	public void setVideoRecord(VideoRecorder videoRecorder) {
		this.videoRecorder = videoRecorder;
	}

	public VideoRecorder videoRecorder;

	public interface VideoRecorder {
		abstract public void VideoRecordData(int type, byte[] videodata, int width, int height, int time);
	}

	/**
	 * 获取警笛和白光状态
	 * @param did：设备id
	 * @param pwd：密码
	 */
	public static void getLightAndSirenStatte(String did, String pwd) {
		NativeCaller.TransferMessage(did, "trans_cmd_string.cgi?cmd=2109&command=2" + "&loginuse=" + "admin" + "&loginpas=" + pwd, 1);
	}

	/**
	 * 启动后台线程
	 */
	private void startBackgroundThread() {
		//开启名字为cameraBackground的子线程
		backgroundThread = new HandlerThread(HANDLE_THREAD_NAME);
		backgroundThread.start();
		//将Handle绑定backgroundThread
		backgroundHandler = new Handler(backgroundThread.getLooper());
		//同步锁
		synchronized (lock) {
			runClassifier = true;
		}

		backgroundHandler.post(periodicClassify);
	}

	/**
	 * 停止后台线程
	 */
	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
	private void stopBackgroundThread() {
		backgroundThread.quitSafely();
		try {
			backgroundThread.join();
			backgroundThread = null;
			backgroundHandler = null;
			synchronized (lock) {
				runClassifier = false;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 定期拍照并识别
	 */
	private Runnable periodicClassify =
			new Runnable() {
				@Override
				public void run() {
					synchronized (lock) {
						if (runClassifier) {
							classifyFrame();
						}
					}
					backgroundHandler.post(periodicClassify);
				}
			};

	/**
	 * tensorFlow识别，判定使用手机和吸烟
	 */
	private void classifyFrame() {
		if (classifier == null || initPosition || videodata == null) {
			return;
		}
		byte[] rgb = new byte[nVideoWidths * nVideoHeights * 2];
		NativeCaller.YUV4202RGB565(videodata, rgb, nVideoWidths, nVideoHeights);
		ByteBuffer buffer = ByteBuffer.wrap(rgb);
		Bitmap tmpBitmap = Bitmap.createBitmap( nVideoWidths, nVideoHeights, Bitmap.Config.RGB_565);
		tmpBitmap.copyPixelsFromBuffer(buffer);
		Bitmap recognBitmap = changeBitmapSize(tmpBitmap);
		final List<Classifier.Recognition> results = classifier.recognizeImage(recognBitmap);
		tmpBitmap.recycle();
		for (final Classifier.Recognition result : results) {
			final RectF location = result.getLocation();
			if (location != null && result.getConfidence() >= MINIMUM_CONFIDENCE_TF_OD_API) {
				if (result.getTitle().equals("phone")) {
					makePhoneAlarm();
				} else if (result.getTitle().equals("smoke")) {
					makeSmokingAlarm();
				}
			}
		}
	}
	//改变Bitmap的大小，使其适应tensorflow的模型
	private Bitmap changeBitmapSize(Bitmap bitmap) {
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		int newWidth=TF_OD_API_INPUT_SIZE;
		int newHeight=TF_OD_API_INPUT_SIZE;
		//计算压缩的比率
		float scaleWidth=((float)newWidth)/width;
		float scaleHeight=((float)newHeight)/height;
		// 获取想要缩放的matrix
		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth,scaleHeight);

		//获取新的bitmap
		bitmap=Bitmap.createBitmap(bitmap,0,0,width,height,matrix,true);
		bitmap.getWidth();
		bitmap.getHeight();
		return bitmap;
	}
}
