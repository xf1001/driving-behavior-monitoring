package com.ipcamera.detect.net;


import com.ipcamera.detect.bean.ErrorBean;
import com.ipcamera.detect.bean.JsonBean;

public abstract interface ApiCallBack {
    void onFinish(JsonBean bean);
    void onError(ErrorBean bean);
}
