package com.ipcamera.detect;
import vstc2.nativecaller.NativeCaller;

import com.ipcamer.detect.R;
import com.ipcamera.detect.BridgeService.AlarmInterface;
import com.ipcamera.detect.bean.AlermBean;
import com.ipcamera.detect.utils.ContentCommon;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

public class SettingAlarmActivity extends BaseActivity implements
		OnClickListener, OnCheckedChangeListener, OnGestureListener,
		OnTouchListener, AlarmInterface {
	// private String TAG = "SettingAlermActivity";
	private String strDID = null;
	private boolean successFlag = false;
	private final int TIMEOUT = 3000;
	private final int ALERMPARAMS = 3;
	private final int UPLOADTIMETOOLONG = 4;
	private int cameraType = 0;
	private GestureDetector gt = new GestureDetector(this);
	private Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 0:
				showToast(R.string.alerm_set_failed);
				break;
			case 1:
				showToast(R.string.setting_aler_sucess);
				finish();
				break;
			case ALERMPARAMS:
				successFlag = true;
				progressDialog.cancel();
				if (0 == alermBean.getMotion_armed()) {
					motionAlermView.setVisibility(View.GONE);

				} else {
					cbxMotionAlarm.setChecked(true);
					motionAlermView.setVisibility(View.VISIBLE);
				}
				tvSensitivity.setText(String.valueOf(alermBean
						.getMotion_sensitivity()));
				break;
			default:
				break;
			}
		}
	};

	private Button btnOk = null;
	private Button btnCancel = null;
	private View motionAlermView = null;

	private ImageView imgSensitiveDrop = null;

	private TextView tvSensitivity = null;

	private CheckBox cbxMotionAlarm = null;
	private AlermBean alermBean = null;

	private PopupWindow sensitivePopWindow = null;
	private PopupWindow triggerLevelPopWindow = null;
	private PopupWindow ioOutLevelPopWindow = null;
	private PopupWindow presteMovePopWindow = null;
	private PopupWindow audioPopWindow = null;

	private ProgressDialog progressDialog = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		getDataFromOther();
		setContentView(R.layout.settingalarm);
		NativeCaller.PPPPGetSystemParams(strDID,
				ContentCommon.MSG_TYPE_GET_PARAMS);
		progressDialog = new ProgressDialog(this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(getString(R.string.alerm_getparams));
		progressDialog.show();
		mHandler.postDelayed(runnable, TIMEOUT);
		alermBean = new AlermBean();
		findView();
		setListener();
		BridgeService.setAlarmInterface(this);

		initMovePopupWindow();
	}

	@Override
	protected void onPause() {
		overridePendingTransition(R.anim.out_to_right, R.anim.in_from_left);
		super.onPause();
	}

	private Runnable runnable = new Runnable() {

		@Override
		public void run() {
			if (!successFlag) {
				progressDialog.dismiss();
			}
		}
	};

	private void setListener() {
		btnOk.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		imgSensitiveDrop.setOnClickListener(this);
		cbxMotionAlarm.setOnCheckedChangeListener(this);
		scrollView.setOnTouchListener(this);
		progressDialog.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {

				if (keyCode == KeyEvent.KEYCODE_BACK) {
					return true;
				}
				return false;
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.alerm_ok:
			setAlerm();
			break;
		case R.id.alerm_cancel:
			finish();
			break;
		case R.id.alerm_img_sensitive_drop://
			dismissPopupWindow();
			sensitivePopWindow.showAsDropDown(imgSensitiveDrop, -120, 10);
			break;
		case R.id.sensitive_10:
			sensitivePopWindow.dismiss();
			alermBean.setMotion_sensitivity(10);
			tvSensitivity.setText(String.valueOf(10));
			break;

		case R.id.sensitive_9:
			sensitivePopWindow.dismiss();
			alermBean.setMotion_sensitivity(9);
			tvSensitivity.setText(String.valueOf(9));
			break;

		case R.id.sensitive_8:
			sensitivePopWindow.dismiss();
			alermBean.setMotion_sensitivity(8);
			tvSensitivity.setText(String.valueOf(8));
			break;

		case R.id.sensitive_7:
			sensitivePopWindow.dismiss();
			alermBean.setMotion_sensitivity(7);
			tvSensitivity.setText(String.valueOf(7));
			break;

		case R.id.sensitive_6:
			sensitivePopWindow.dismiss();
			alermBean.setMotion_sensitivity(6);
			tvSensitivity.setText(String.valueOf(6));
			break;
		case R.id.sensitive_5:
			sensitivePopWindow.dismiss();
			alermBean.setMotion_sensitivity(5);
			tvSensitivity.setText(String.valueOf(5));
			break;
		case R.id.sensitive_4:
			sensitivePopWindow.dismiss();
			alermBean.setMotion_sensitivity(4);
			tvSensitivity.setText(String.valueOf(4));
			break;
		case R.id.sensitive_3:
			sensitivePopWindow.dismiss();
			alermBean.setMotion_sensitivity(3);
			tvSensitivity.setText(String.valueOf(3));
			break;
		case R.id.sensitive_2:
			sensitivePopWindow.dismiss();
			alermBean.setMotion_sensitivity(2);
			tvSensitivity.setText(String.valueOf(2));
			break;
		case R.id.sensitive_1:
			sensitivePopWindow.dismiss();
			alermBean.setMotion_sensitivity(1);
			tvSensitivity.setText(String.valueOf(1));
			break;
		default:
			break;
		}
	}

	private void setAlerm() {
		if (successFlag) {
			Log.e("setAlerm", "setAlermTemp: " + alermBean.getAlarm_temp());
			NativeCaller.PPPPAlarmSetting(strDID, alermBean.getAlarm_audio(),
					alermBean.getMotion_armed(),
					alermBean.getMotion_sensitivity(),
					alermBean.getInput_armed(), alermBean.getIoin_level(),
					alermBean.getIoout_level(), alermBean.getIolinkage(),
					alermBean.getAlermpresetsit(), alermBean.getMail(),
					alermBean.getSnapshot(), alermBean.getRecord(),
					alermBean.getUpload_interval(),
					alermBean.getSchedule_enable(),
					0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
					0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
					0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
					0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
					0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
					0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
					0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
					0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
					0xFFFFFFFF,0xFFFFFFFF,-1);
		} else {
			showToast(R.string.alerm_set_failed);
		}
	}

	private ScrollView scrollView;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return gt.onTouchEvent(event);
	}

	private void findView() {

		cbxMotionAlarm = (CheckBox) findViewById(R.id.alerm_cbx_move_layout);

		tvSensitivity = (TextView) findViewById(R.id.alerm_tv_sensitivity);

		imgSensitiveDrop = (ImageView) findViewById(R.id.alerm_img_sensitive_drop);

		motionAlermView = findViewById(R.id.alerm_moveview);

		btnOk = (Button) findViewById(R.id.alerm_ok);
		btnCancel = (Button) findViewById(R.id.alerm_cancel);

		scrollView = (ScrollView) findViewById(R.id.scrollView1);

		Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.top_bg);
		BitmapDrawable drawable = new BitmapDrawable(bitmap);
		drawable.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		drawable.setDither(true);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.alerm_cbx_move_layout:
			if (isChecked) {
				alermBean.setMotion_armed(1);
				motionAlermView.setVisibility(View.VISIBLE);
			} else {
				alermBean.setMotion_armed(0);
				motionAlermView.setVisibility(View.GONE);
			}
			break;
		}
	}

	private void getDataFromOther() {
		Intent intent = getIntent();
		strDID = intent.getStringExtra(ContentCommon.STR_CAMERA_ID);
		cameraType = intent.getIntExtra(ContentCommon.STR_CAMERA_TYPE, 0);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		dismissPopupWindow();
	}

	@Override
	public boolean onDown(MotionEvent e) {
		dismissPopupWindow();
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		return false;
	}

	@Override
	public void callBackAlarmParams(String did, int alarm_audio, int motion_armed,
			int motion_sensitivity, int input_armed, int ioin_level,
			int iolinkage, int ioout_level, int alarmpresetsit, int mail,
			int snapshot, int record, int upload_interval,
			int schedule_enable, int schedule_sun_0, int schedule_sun_1,
			int schedule_sun_2, int schedule_mon_0, int schedule_mon_1,
			int schedule_mon_2, int schedule_tue_0, int schedule_tue_1,
			int schedule_tue_2, int schedule_wed_0, int schedule_wed_1,
			int schedule_wed_2, int schedule_thu_0, int schedule_thu_1,
			int schedule_thu_2, int schedule_fri_0, int schedule_fri_1,
			int schedule_fri_2, int schedule_sat_0, int schedule_sat_1,
			int schedule_sat_2) {

		alermBean.setDid(did);
		alermBean.setMotion_armed(motion_armed);
		alermBean.setMotion_sensitivity(motion_sensitivity);
		alermBean.setInput_armed(input_armed);
		alermBean.setIoin_level(ioin_level);
		alermBean.setIolinkage(iolinkage);
		alermBean.setIoout_level(ioout_level);
		alermBean.setAlermpresetsit(alarmpresetsit);
		alermBean.setMail(mail);
		alermBean.setSnapshot(snapshot);
		alermBean.setRecord(record);
		alermBean.setUpload_interval(upload_interval);
		alermBean.setAlarm_audio(alarm_audio);
		alermBean.setAlarm_temp(input_armed);
		alermBean.setSchedule_enable(schedule_enable);
		mHandler.sendEmptyMessage(ALERMPARAMS);
	}

	@Override
	public void callBackSetSystemParamsResult(String did, int paramType,
			int result) {
		mHandler.sendEmptyMessage(result);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		dismissPopupWindow();
		return false;
	}

	private void dismissPopupWindow() {
		if (presteMovePopWindow != null && presteMovePopWindow.isShowing()) {
			presteMovePopWindow.dismiss();
		}
		if (sensitivePopWindow != null && sensitivePopWindow.isShowing()) {
			sensitivePopWindow.dismiss();
		}
		if (triggerLevelPopWindow != null && triggerLevelPopWindow.isShowing()) {
			triggerLevelPopWindow.dismiss();
		}
		if (ioOutLevelPopWindow != null && ioOutLevelPopWindow.isShowing()) {
			ioOutLevelPopWindow.dismiss();
		}
		if (audioPopWindow != null && audioPopWindow.isShowing()) {
			audioPopWindow.dismiss();
		}
	}

	private void initMovePopupWindow() {

		LinearLayout layout1 = (LinearLayout) LayoutInflater.from(this)
				.inflate(R.layout.alermsensitivepopwindow, null);
		TextView sensitive10 = (TextView) layout1
				.findViewById(R.id.sensitive_10);
		TextView sensitive9 = (TextView) layout1.findViewById(R.id.sensitive_9);
		TextView sensitive8 = (TextView) layout1.findViewById(R.id.sensitive_8);
		TextView sensitive7 = (TextView) layout1.findViewById(R.id.sensitive_7);
		TextView sensitive6 = (TextView) layout1.findViewById(R.id.sensitive_6);
		TextView sensitive5 = (TextView) layout1.findViewById(R.id.sensitive_5);
		TextView sensitive4 = (TextView) layout1.findViewById(R.id.sensitive_4);
		TextView sensitive3 = (TextView) layout1.findViewById(R.id.sensitive_3);
		TextView sensitive2 = (TextView) layout1.findViewById(R.id.sensitive_2);
		TextView sensitive1 = (TextView) layout1.findViewById(R.id.sensitive_1);
		sensitive10.setOnClickListener(this);
		sensitive9.setOnClickListener(this);
		sensitive8.setOnClickListener(this);
		sensitive7.setOnClickListener(this);
		sensitive6.setOnClickListener(this);
		sensitive5.setOnClickListener(this);
		sensitive4.setOnClickListener(this);
		sensitive3.setOnClickListener(this);
		sensitive2.setOnClickListener(this);
		sensitive1.setOnClickListener(this);
		sensitivePopWindow = new PopupWindow(layout1, 160,
				WindowManager.LayoutParams.WRAP_CONTENT);
	}
}
