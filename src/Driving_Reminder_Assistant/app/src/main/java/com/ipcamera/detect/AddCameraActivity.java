package com.ipcamera.detect;

import java.util.Map;

import vstc2.nativecaller.NativeCaller;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.ipcamer.detect.R;
import com.ipcamera.detect.BridgeService.AddCameraInterface;
import com.ipcamera.detect.BridgeService.CallBackMessageInterface;
import com.ipcamera.detect.BridgeService.IpcamClientInterface;
import com.ipcamera.detect.adapter.SearchListAdapter;
import com.ipcamera.detect.utils.ContentCommon;
import com.ipcamera.detect.utils.MySharedPreferenceUtil;
import com.ipcamera.detect.utils.MyStringUtils;
import com.ipcamera.detect.utils.SystemValue;
import com.ipcamera.detect.utils.VuidUtils;

public class AddCameraActivity extends Activity implements OnClickListener,AddCameraInterface, OnItemSelectedListener, IpcamClientInterface,CallBackMessageInterface
{
	private EditText userEdit = null;
	private EditText pwdEdit = null;
	private EditText didEdit = null;
	private TextView textView_top_show = null;
	private Button button_conn;
	private static final int SEARCH_TIME = 3000;
	private int option = ContentCommon.INVALID_OPTION;
	private int CameraType = ContentCommon.CAMERA_TYPE_MJPEG;
	private Button btnSearchCamera;
	private SearchListAdapter listAdapter = null;
	private ProgressDialog progressdlg = null;
	private boolean isSearched;
	private MyBroadCast receiver;
	private WifiManager manager = null;
	private ProgressBar progressBar = null;
	private static final String STR_DID = "did";
	private static final String STR_MSG_PARAM = "msgparam";
	private MyWifiThread myWifiThread = null;
	private boolean blagg = false;
	private Intent intentbrod = null;
	private Button button_play = null;
	private Button button_setting = null;
	private Button btn_info;
	private int tag = 0;


	class MyWifiThread extends Thread {
		@Override
		public void run() {
			while (blagg == true) {
				super.run();
				updateListHandler.sendEmptyMessage(100000);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private class MyBroadCast extends BroadcastReceiver {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			AddCameraActivity.this.finish();
			Log.d("ip", "AddCameraActivity.this.finish()");
		}

	}

	class StartPPPPThread implements Runnable {
		@Override
		public void run() {
			try {
				Thread.sleep(100);
				if(VuidUtils.isVuid(SystemValue.deviceId))
				{
					int vuidStatus = NativeCaller.StartVUID("0",SystemValue.devicePass,1,"","",0,SystemValue.deviceId,0);
                    Log.e("vst","vuidStatus"+vuidStatus);
                    if(vuidStatus == -2)
					{
                        // TODO: VUID  无效
						Bundle bd = new Bundle();
						Message msg = PPPPMsgHandler.obtainMessage();
						msg.what = ContentCommon.PPPP_MSG_VSNET_NOTIFY_TYPE_VUIDSTATUS;
						bd.putInt(STR_MSG_PARAM, -2);
						bd.putString(STR_DID, SystemValue.deviceId);
						msg.setData(bd);
						PPPPMsgHandler.sendMessage(msg);
					}
				}else {
					startCameraPPPP();
				}
			} catch (Exception e) {

			}
		}
	}
	private void startCameraPPPP() {
		try {
			Thread.sleep(100);
		} catch (Exception e) {
		}

		if(SystemValue.deviceId.toLowerCase().startsWith("vsta"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","EFGFFBBOKAIEGHJAEDHJFEEOHMNGDCNJCDFKAKHLEBJHKEKMCAFCDLLLHAOCJPPMBHMNOMCJKGJEBGGHJHIOMFBDNPKNFEGCEGCBGCALMFOHBCGMFK",0);
		}else if(SystemValue.deviceId.toLowerCase().startsWith("vstd"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","HZLXSXIALKHYEIEJHUASLMHWEESUEKAUIHPHSWAOSTEMENSQPDLRLNPAPEPGEPERIBLQLKHXELEHHULOEGIAEEHYEIEK-$$",1);
		}else if(SystemValue.deviceId.toLowerCase().startsWith("vstf"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","HZLXEJIALKHYATPCHULNSVLMEELSHWIHPFIBAOHXIDICSQEHENEKPAARSTELERPDLNEPLKEILPHUHXHZEJEEEHEGEM-$$",1);
		}
		else if(SystemValue.deviceId.toLowerCase().startsWith("vste"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","EEGDFHBAKKIOGNJHEGHMFEEDGLNOHJMPHAFPBEDLADILKEKPDLBDDNPOHKKCIFKJBNNNKLCPPPNDBFDL",0);
    	}else if(SystemValue.deviceId.toLowerCase().startsWith("pisr"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","EFGFFBBOKAIEGHJAEDHJFEEOHMNGDCNJCDFKAKHLEBJHKEKMCAFCDLLLHAOCJPPMBHMNOMCJKGJEBGGHJHIOMFBDNPKNFEGCEGCBGCALMFOHBCGMFK",0);
		}
     	else if(SystemValue.deviceId.toLowerCase().startsWith("vstg"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","EEGDFHBOKCIGGFJPECHIFNEBGJNLHOMIHEFJBADPAGJELNKJDKANCBPJGHLAIALAADMDKPDGOENEBECCIK:vstarcam2018",0);
		}else if(SystemValue.deviceId.toLowerCase().startsWith("vsth"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","EEGDFHBLKGJIGEJLEKGOFMEDHAMHHJNAGGFABMCOBGJOLHLJDFAFCPPHGILKIKLMANNHKEDKOINIBNCPJOMK:vstarcam2018",0);
		}else if(SystemValue.deviceId.toLowerCase().startsWith("vstb")||SystemValue.deviceId.toLowerCase().startsWith("vstc"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","ADCBBFAOPPJAHGJGBBGLFLAGDBJJHNJGGMBFBKHIBBNKOKLDHOBHCBOEHOKJJJKJBPMFLGCPPJMJAPDOIPNL",0);
		}

		else if(SystemValue.deviceId.toLowerCase().startsWith("vstj"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","EEGDFHBLKGJIGEJNEOHEFBEIGANCHHMBHIFEAHDEAMJCKCKJDJAFDDPPHLKJIHLMBENHKDCHPHNJBODA:vstarcam2019",0);

		}
		//改成63，方便手机接入
		else if(SystemValue.deviceId.toLowerCase().startsWith("vstk"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","EBGDEJBJKGJFGJJBEFHPFCEKHGNMHNNMHMFFBICPAJJNLDLLDHACCNONGLLPJGLKANMJLDDHODMEBOCIJEMA:vstarcam2019",0);

		}
		else if(SystemValue.deviceId.toLowerCase().startsWith("vstm"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","EBGEEOBOKHJNHGJGEAGAEPEPHDMGHINBGIECBBCBBJIKLKLCCDBBCFODHLKLJJKPBOMELECKPNMNAICEJCNNJH:vstarcam2019",0);

		}
		else if(SystemValue.deviceId.toLowerCase().startsWith("vstn"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","EEGDFHBBKBIFGAIAFGHDFLFJGJNIGEMOHFFPAMDMAAIIKBKNCDBDDMOGHLKCJCKFBFMPLMCBPEMG:vstarcam2019",0);

		}
		else if(SystemValue.deviceId.toLowerCase().startsWith("vstl"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","EEGDFHBLKGJIGEJIEIGNFPEEHGNMHPNBGOFIBECEBLJDLMLGDKAPCNPFGOLLJFLJAOMKLBDFOGMAAFCJJPNFJP:vstarcam2019",0);

		}
		else if(SystemValue.deviceId.toLowerCase().startsWith("vstp"))
		{
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","EEGDFHBLKGJIGEJLEIGJFLENHLNBHCNMGAFGBNCOAIJMLKKODNALCCPKGBLHJLLHAHMBKNDFOGNGBDCIJFMB:vstarcam2019",0);

		}
		else {
			NativeCaller.StartPPPPExt(SystemValue.deviceId, SystemValue.deviceName,
					SystemValue.devicePass,63,"","",0);
		}
	}
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			Toast.makeText(this, "Permission GET", Toast.LENGTH_SHORT).show();
		} else {
//             Permission Denied
			Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
		}
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}
	private void stopCameraPPPP()
	{
		NativeCaller.StopPPPP(SystemValue.deviceId);
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActivityCompat.requestPermissions(this,
				new String[]{
						Manifest.permission.RECORD_AUDIO,
						Manifest.permission.CAMERA,
						Manifest.permission.WRITE_EXTERNAL_STORAGE,
						Manifest.permission.READ_EXTERNAL_STORAGE,
						Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS}, 1);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.add_camera);
		progressdlg = new ProgressDialog(this);
		progressdlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressdlg.setMessage(getString(R.string.searching_tip));
		listAdapter = new SearchListAdapter(this);
		findView();
		manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		InitParams();
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		BridgeService.setAddCameraInterface(this);
		BridgeService.setCallBackMessage(this);
		receiver = new MyBroadCast();
		IntentFilter filter = new IntentFilter();
		filter.addAction("finish");
		registerReceiver(receiver, filter);
		intentbrod = new Intent("drop");
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		blagg = true;
	}

	private void InitParams() {
		button_conn.setOnClickListener(this);
		btnSearchCamera.setOnClickListener(this);
		//监听软键盘的删除键
		userEdit.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_DEL) {
						String s = userEdit.getText().toString();
						if (TextUtils.isEmpty(s)) {
							return true;
						}
					}
				return false;
				}
			});
		//监听软键盘的删除键
		pwdEdit.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_DEL) {
					String s = userEdit.getText().toString();
					if (TextUtils.isEmpty(s)) {
						return true;
					}
				}
				return false;
			}
		});
		//监听软键盘的删除键
		didEdit.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_DEL) {
					String s = userEdit.getText().toString();
					if (TextUtils.isEmpty(s)) {
						return true;
					}
				}
				return false;
			}
		});
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (myWifiThread != null) {
			blagg = false;
		}
		progressdlg.dismiss();
		NativeCaller.StopSearch();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
		NativeCaller.Free();
		Intent intent = new Intent();
		intent.setClass(this, BridgeService.class);
		stopService(intent);
		tag = 0;
	}

	Runnable updateThread = new Runnable() {

		public void run() {
			NativeCaller.StopSearch();
			progressdlg.dismiss();
			Message msg = updateListHandler.obtainMessage();
			msg.what = 1;
			updateListHandler.sendMessage(msg);
		}
	};

	Handler updateListHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				listAdapter.notifyDataSetChanged();
				if (listAdapter.getCount() > 0) {
					AlertDialog.Builder dialog = new AlertDialog.Builder(
							AddCameraActivity.this);
					dialog.setTitle(getResources().getString(
							R.string.add_search_result));
					dialog.setPositiveButton(
							getResources().getString(R.string.refresh),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									startSearch();
								}
							});
					dialog.setNegativeButton(
							getResources().getString(R.string.str_cancel), null);
					dialog.setAdapter(listAdapter,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int arg2) {
									Map<String, Object> mapItem = (Map<String, Object>) listAdapter.getItemContent(arg2);
									if (mapItem == null) {
										return;
									}
									String strName = (String) mapItem
											.get(ContentCommon.STR_CAMERA_NAME);
									String strDID = (String) mapItem
											.get(ContentCommon.STR_CAMERA_ID);
									String strUser = ContentCommon.DEFAULT_USER_NAME;
									String strPwd = ContentCommon.DEFAULT_USER_PWD;
									userEdit.setText(strUser);
									pwdEdit.setText(strPwd);
									didEdit.setText(strDID);
								}
							});

					dialog.show();
				} else {
					Toast.makeText(AddCameraActivity.this,
							getResources().getString(R.string.add_search_no),
							Toast.LENGTH_LONG).show();
					isSearched = false;//
				}
			}
		}
	};


	private void startSearch() {
		listAdapter.ClearAll();
		progressdlg.setMessage(getString(R.string.searching_tip));
		progressdlg.show();
		new Thread(new SearchThread()).start();
		updateListHandler.postDelayed(updateThread, SEARCH_TIME);
	}

	private class SearchThread implements Runnable {
		@Override
		public void run() {
			Log.d("tag", "startSearch");
			NativeCaller.StartSearch();
		}
	}

	private void findView() {
		progressBar = (ProgressBar) findViewById(R.id.main_model_progressBar1);
		textView_top_show = (TextView) findViewById(R.id.login_textView1);
		button_play = (Button) findViewById(R.id.play);
		button_setting = (Button) findViewById(R.id.setting);
		button_conn = (Button) findViewById(R.id.done);
		//done.setText("连接");
		userEdit = (EditText) findViewById(R.id.editUser);

		pwdEdit = (EditText) findViewById(R.id.editPwd);
		didEdit = (EditText) findViewById(R.id.editDID);
		btnSearchCamera = (Button) findViewById(R.id.btn_searchCamera);

		btn_info = (Button)findViewById(R.id.getInfo);
		button_play.setOnClickListener(this);
		button_setting.setOnClickListener(this);

		btn_info.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.play:
				if (tag == 1) {
					Intent intent = new Intent(AddCameraActivity.this, PlayActivity.class);
					startActivity(intent);
				}else{
					Toast.makeText(AddCameraActivity.this,getResources().getString(R.string.main_play_prompt),Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.setting:
				if (tag == 1) {
					Intent intent1 = new Intent(AddCameraActivity.this,
							SettingActivity.class);
					intent1.putExtra(ContentCommon.STR_CAMERA_ID,
							SystemValue.deviceId);
					intent1.putExtra(ContentCommon.STR_CAMERA_NAME,
							SystemValue.deviceName);
					intent1.putExtra(ContentCommon.STR_CAMERA_PWD, SystemValue.devicePass);
					startActivity(intent1);
					overridePendingTransition(R.anim.in_from_right,
							R.anim.out_to_left);
				} else {
					Toast.makeText(AddCameraActivity.this,getResources().getString(R.string.main_setting_prompt),Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.done:
				if (tag == 1) {
					Toast.makeText(AddCameraActivity.this,"设备已经是在线状态了",Toast.LENGTH_SHORT).show();
				} else if (tag == 2) {
					Toast.makeText(AddCameraActivity.this, "设备不在线",Toast.LENGTH_SHORT).show();
				} else {
					loginAndConnect();
				}
				break;
			case R.id.btn_searchCamera:
				stopCameraPPPP();
				//把相机状态，设备id置空
				tag=0;
				textView_top_show.setText(R.string.login_stuta_camer);
				SystemValue.deviceId=null;
				searchCamera();
				break;
			case R.id.getInfo:
				//todo 驾驶信息收集
				startActivity(new Intent(AddCameraActivity.this,MessageActivity.class));
				break;
		default:
			break;
		}
	}
	

	private void searchCamera() {
		if (!isSearched) {
			isSearched = true;
			startSearch();
		} else {
			AlertDialog.Builder dialog = new AlertDialog.Builder(
					AddCameraActivity.this);
			dialog.setTitle(getResources()
					.getString(R.string.add_search_result));
			dialog.setPositiveButton(
					getResources().getString(R.string.refresh),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							startSearch();

						}
					});
			dialog.setNegativeButton(
					getResources().getString(R.string.str_cancel), null);
			dialog.setAdapter(listAdapter,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int arg2) {
							Map<String, Object> mapItem = (Map<String, Object>) listAdapter.getItemContent(arg2);
							if (mapItem == null) {
								return;
							}
							String strName = (String) mapItem
									.get(ContentCommon.STR_CAMERA_NAME);
							String strDID = (String) mapItem
									.get(ContentCommon.STR_CAMERA_ID);
							String strUser = ContentCommon.DEFAULT_USER_NAME;
							String strPwd = ContentCommon.DEFAULT_USER_PWD;
							userEdit.setText(strUser);
							pwdEdit.setText(strPwd);
							didEdit.setText(strDID);
						}
					});
			dialog.show();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			AddCameraActivity.this.finish();
			return false;
		}
		return false;
	}

	private void loginAndConnect() {
		Intent in = new Intent();
		String strUser = userEdit.getText().toString();
		String strPwd = pwdEdit.getText().toString();
		String strDID = didEdit.getText().toString();

		if (strDID.length() == 0) 
		{
			Toast.makeText(AddCameraActivity.this,
					getResources().getString(R.string.input_camera_id), Toast.LENGTH_SHORT).show();
			return;
		}
		if (strUser.length() == 0)
		{
			Toast.makeText(AddCameraActivity.this,
					getResources().getString(R.string.input_camera_user), Toast.LENGTH_SHORT).show();
			return;
		}
		if (option == ContentCommon.INVALID_OPTION) {
			option = ContentCommon.ADD_CAMERA;
		}
		in.putExtra(ContentCommon.CAMERA_OPTION, option);
		in.putExtra(ContentCommon.STR_CAMERA_ID, strDID);
		in.putExtra(ContentCommon.STR_CAMERA_USER, strUser);
		in.putExtra(ContentCommon.STR_CAMERA_PWD, strPwd);
		in.putExtra(ContentCommon.STR_CAMERA_TYPE, CameraType);
		progressBar.setVisibility(View.VISIBLE);
		SystemValue.deviceName = strUser;
		SystemValue.deviceId = strDID;
		SystemValue.devicePass = strPwd;
		BridgeService.setIpcamClientInterface(this);
		NativeCaller.Init();
		new Thread(new StartPPPPThread()).start();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			Bundle bundle = data.getExtras();
			String scanResult = bundle.getString("result");
			didEdit.setText(scanResult);
		}
	}

	/**
	 * BridgeService callback
	 * **/
	@Override
	public void callBackSearchResultData(int sysver, String strMac,
			String strName, String strDeviceID, String strIpAddr, int port) {
		Log.e("AddCameraActivity", strDeviceID+strName);
		if (!listAdapter.AddCamera(strMac, strName, strDeviceID)) {
			return;
		}
	}

	private Handler PPPPMsgHandler = new Handler() {
		public void handleMessage(Message msg) {

			Bundle bd = msg.getData();
			int msgParam = bd.getInt(STR_MSG_PARAM);
			int msgType = msg.what;
			Log.i("aaa", "===="+msgType+"--msgParam:"+msgParam);
			String did = bd.getString(STR_DID);
			int resid = R.string.pppp_status_connecting;
			switch (msgType) {
			case ContentCommon.PPPP_MSG_TYPE_PPPP_STATUS:

				switch (msgParam) {
				case ContentCommon.PPPP_STATUS_CONNECTING://0
					resid = R.string.pppp_status_connecting;
					progressBar.setVisibility(View.VISIBLE);
					tag = 2;
					break;
				case ContentCommon.PPPP_STATUS_CONNECT_FAILED://3
					resid = R.string.pppp_status_connect_failed;
					progressBar.setVisibility(View.GONE);
					tag = 0;
					break;
				case ContentCommon.PPPP_STATUS_DISCONNECT://4
					resid = R.string.pppp_status_disconnect;
					progressBar.setVisibility(View.GONE);
					tag = 0;
					break;
				case ContentCommon.PPPP_STATUS_INITIALING://1
					resid = R.string.pppp_status_initialing;
					progressBar.setVisibility(View.VISIBLE);
					tag = 2;
					break;
				case ContentCommon.PPPP_STATUS_INVALID_ID://5
					resid = R.string.pppp_status_invalid_id;
					progressBar.setVisibility(View.GONE);
					tag = 0;
					break;
				case ContentCommon.PPPP_STATUS_ON_LINE://2 在线状态
					resid = R.string.pppp_status_online;
					progressBar.setVisibility(View.GONE);
					//摄像机在线之后读取摄像机类型
					String cmd="get_status.cgi?loginuse=admin&loginpas=" + SystemValue.devicePass
							+ "&user=admin&pwd=" + SystemValue.devicePass;
					NativeCaller.TransferMessage(did, cmd, 1);
					tag = 1;
					break;
				case ContentCommon.PPPP_STATUS_DEVICE_NOT_ON_LINE://6
					resid = R.string.device_not_on_line;
					progressBar.setVisibility(View.GONE);
					tag = 0;
					break;
				case ContentCommon.PPPP_STATUS_CONNECT_TIMEOUT://7
					resid = R.string.pppp_status_connect_timeout;
					progressBar.setVisibility(View.GONE);
					tag = 0;
					break;
				case ContentCommon.PPPP_STATUS_CONNECT_ERRER://8
					resid =R.string.pppp_status_pwd_error;
					progressBar.setVisibility(View.GONE);
					tag = 0;
					break;
				default:
					resid = R.string.pppp_status_unknown;
				}
				textView_top_show.setText(getResources().getString(resid));
				if (msgParam == ContentCommon.PPPP_STATUS_ON_LINE) {
					NativeCaller.PPPPGetSystemParams(did, ContentCommon.MSG_TYPE_GET_PARAMS);
					NativeCaller.TransferMessage(did,
							"get_factory_param.cgi?loginuse=admin&loginpas="
									+ SystemValue.devicePass + "&user=admin&pwd=" + SystemValue.devicePass, 1);// 检测push值
				}
				if (msgParam == ContentCommon.PPPP_STATUS_INVALID_ID
						|| msgParam == ContentCommon.PPPP_STATUS_CONNECT_FAILED
						|| msgParam == ContentCommon.PPPP_STATUS_DEVICE_NOT_ON_LINE
						|| msgParam == ContentCommon.PPPP_STATUS_CONNECT_TIMEOUT
						|| msgParam == ContentCommon.PPPP_STATUS_CONNECT_ERRER) {
					NativeCaller.StopPPPP(did);
				}
				break;
			case ContentCommon.PPPP_MSG_TYPE_PPPP_MODE:

				break;
			case ContentCommon.PPPP_MSG_VSNET_NOTIFY_TYPE_VUIDSTATUS:

                switch (msgParam) {
                    case ContentCommon.PPPP_STATUS_CONNECTING://0
                        resid = R.string.pppp_status_connecting;
                        progressBar.setVisibility(View.VISIBLE);
                        tag = 2;
                        break;
                    case ContentCommon.PPPP_STATUS_CONNECT_FAILED://3
                        resid = R.string.pppp_status_connect_failed;
                        progressBar.setVisibility(View.GONE);
                        tag = 0;
                        break;
                    case ContentCommon.PPPP_STATUS_DISCONNECT://4
                        resid = R.string.pppp_status_disconnect;
                        progressBar.setVisibility(View.GONE);
                        tag = 0;
                        break;
                    case ContentCommon.PPPP_STATUS_INITIALING://1
                        resid = R.string.pppp_status_initialing;
                        progressBar.setVisibility(View.VISIBLE);
                        tag = 2;
                        break;
                    case ContentCommon.PPPP_STATUS_INVALID_ID://5
                        resid = R.string.pppp_status_invalid_id;
                        progressBar.setVisibility(View.GONE);
                        tag = 0;
                        break;
                    case ContentCommon.PPPP_STATUS_ON_LINE://2 在线状态
                        resid = R.string.pppp_status_online;
                        progressBar.setVisibility(View.GONE);
                        //摄像机在线之后读取摄像机类型
                        String cmd="get_status.cgi?loginuse=admin&loginpas=" + SystemValue.devicePass
                                + "&user=admin&pwd=" + SystemValue.devicePass;
                        NativeCaller.TransferMessage(did, cmd, 1);
                        tag = 1;
                        break;
                    case ContentCommon.PPPP_STATUS_DEVICE_NOT_ON_LINE://6
                        resid = R.string.device_not_on_line;
                        progressBar.setVisibility(View.GONE);
                        tag = 0;
                        break;
                    case ContentCommon.PPPP_STATUS_CONNECT_TIMEOUT://7
                        resid = R.string.pppp_status_connect_timeout;
                        progressBar.setVisibility(View.GONE);
                        tag = 0;
                        break;
                    case ContentCommon.PPPP_STATUS_CONNECT_ERRER://8
                        resid =R.string.pppp_status_pwd_error;
                        progressBar.setVisibility(View.GONE);
                        tag = 0;
                        break;
                    case ContentCommon.PPPP_STATUS_INVALID_VUID:
                        resid =R.string.pppp_status_invalid_id;
                        progressBar.setVisibility(View.GONE);
                        tag = 0;
                        break;
					case ContentCommon.PPPP_STATUS_ALLOT_VUID:

						break;

                    default:
                        resid = R.string.pppp_status_unknown;
                }
                textView_top_show.setText(getResources().getString(resid));
                if (msgParam == ContentCommon.PPPP_STATUS_ON_LINE) {
                    NativeCaller.PPPPGetSystemParams(did, ContentCommon.MSG_TYPE_GET_PARAMS);
                    NativeCaller.TransferMessage(did,
                            "get_factory_param.cgi?loginuse=admin&loginpas="
                                    + SystemValue.devicePass + "&user=admin&pwd=" + SystemValue.devicePass, 1);// 检测push值
                }
                if (msgParam == ContentCommon.PPPP_STATUS_INVALID_ID
                        || msgParam == ContentCommon.PPPP_STATUS_CONNECT_FAILED
                        || msgParam == ContentCommon.PPPP_STATUS_DEVICE_NOT_ON_LINE
                        || msgParam == ContentCommon.PPPP_STATUS_CONNECT_TIMEOUT
                        || msgParam == ContentCommon.PPPP_STATUS_CONNECT_ERRER) {
                    NativeCaller.StopPPPP(did);
                }
                break;
			}
		}
	};

	@Override
	public void BSMsgNotifyData(String did, int type, int param) {
		Log.d("ip", "type:" + type + " param:" + param);
		Bundle bd = new Bundle();
		Message msg = PPPPMsgHandler.obtainMessage();
		msg.what = type;
		bd.putInt(STR_MSG_PARAM, param);
		bd.putString(STR_DID, did);
		msg.setData(bd);
		PPPPMsgHandler.sendMessage(msg);
		if (type == ContentCommon.PPPP_MSG_TYPE_PPPP_STATUS) {
			intentbrod.putExtra("ifdrop", param);
			sendBroadcast(intentbrod);
		}

	}

	@Override
	public void BSSnapshotNotify(String did, byte[] bImage, int len) {
		// TODO Auto-generated method stub
		Log.i("ip", "BSSnapshotNotify---len"+len);
	}

	@Override
	public void callBackUserParams(String did, String user1, String pwd1,
			String user2, String pwd2, String user3, String pwd3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void CameraStatus(String did, int status) {

	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void CallBackGetStatus(String did, String resultPbuf, int cmd) {
		// TODO Auto-generated method stub
		if (cmd == ContentCommon.CGI_IEGET_STATUS) {
			String cameraType = spitValue(resultPbuf, "upnp_status=");

			String cameraSysver = MyStringUtils.spitValue(resultPbuf, "sys_ver=");
			MySharedPreferenceUtil.saveSystemVer(AddCameraActivity.this, did, cameraSysver);
			int intType = Integer.parseInt(cameraType);
			int type14 = (int) (intType >> 16) & 1;// 14位 来判断是否报警联动摄像机
			if (intType == 2147483647) {// 特殊值
				type14 = 0;
			}
			if(type14==1){
				updateListHandler.sendEmptyMessage(2);
			}
		}
	}
	
	private String spitValue(String name, String tag) {
		String[] strs = name.split(";");
		for (int i = 0; i < strs.length; i++) {
			String str1 = strs[i].trim();
			if (str1.startsWith("var")) {
				str1 = str1.substring(4, str1.length());
			}
			if (str1.startsWith(tag)) {
				String result = str1.substring(str1.indexOf("=") + 1);
				return result;
			}
		}
		return -1 + "";
	}
}
