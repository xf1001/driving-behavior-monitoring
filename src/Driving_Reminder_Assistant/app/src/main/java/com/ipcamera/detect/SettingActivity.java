package com.ipcamera.detect;
import com.ipcamer.detect.R;
import com.ipcamera.detect.utils.ContentCommon;
import com.ipcamera.detect.utils.SystemValue;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

public class SettingActivity extends Activity implements OnClickListener
{

	private String strDID;
	private String cameraName;
	private String cameraPwd;
	//控件声明
	private RelativeLayout pwd_Relat,alarm_Relat,sd_Relat;
	private Button back_btn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.setting);
		getDataFromOther();
		initView();		
	}

	//获取activity传过来的数据
	private void getDataFromOther()
	{
		Intent intent = getIntent();
		strDID = intent.getStringExtra(ContentCommon.STR_CAMERA_ID);
		cameraName = intent.getStringExtra(ContentCommon.STR_CAMERA_NAME);
		cameraPwd=intent.getStringExtra(ContentCommon.STR_CAMERA_PWD);
	}
	//初始化控件
	private void initView()
	{
		pwd_Relat=(RelativeLayout) findViewById(R.id.pwd_setting);
		alarm_Relat=(RelativeLayout) findViewById(R.id.alarm_setting);
		sd_Relat=(RelativeLayout) findViewById(R.id.sd_setting);
		back_btn=(Button) findViewById(R.id.back);

		pwd_Relat.setOnClickListener(this);
		alarm_Relat.setOnClickListener(this);
		sd_Relat.setOnClickListener(this);
		back_btn.setOnClickListener(this);
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId())
	    {
		case R.id.pwd_setting:
			Intent intent2 = new Intent(SettingActivity.this,SettingUserActivity.class);
			intent2.putExtra(ContentCommon.STR_CAMERA_ID,SystemValue.deviceId);
			intent2.putExtra(ContentCommon.STR_CAMERA_NAME,SystemValue.deviceName);
			intent2.putExtra(ContentCommon.STR_CAMERA_PWD, SystemValue.devicePass);
			startActivity(intent2);
			overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);	
					break;
		case R.id.alarm_setting:
			Intent intent3 = new Intent(SettingActivity.this,SettingAlarmActivity.class);
			intent3.putExtra(ContentCommon.STR_CAMERA_ID,SystemValue.deviceId);
			intent3.putExtra(ContentCommon.STR_CAMERA_NAME,SystemValue.deviceName);
			intent3.putExtra(ContentCommon.STR_CAMERA_PWD, SystemValue.devicePass);
			startActivity(intent3);
			overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
			break;
		case R.id.sd_setting:
			Intent intent5= new Intent(SettingActivity.this,SettingSDCardActivity.class);
			intent5.putExtra(ContentCommon.STR_CAMERA_ID,SystemValue.deviceId);
			intent5.putExtra(ContentCommon.STR_CAMERA_NAME,SystemValue.deviceName);
			intent5.putExtra(ContentCommon.STR_CAMERA_PWD, SystemValue.devicePass);
			startActivity(intent5);
			overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
			break;
		case R.id.back:
			finish();
			overridePendingTransition(R.anim.out_to_right, R.anim.in_from_left);
		default:
			break;
		}
	}
	
}