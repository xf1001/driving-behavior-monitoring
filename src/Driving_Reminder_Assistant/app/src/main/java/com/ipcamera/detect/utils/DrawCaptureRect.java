package com.ipcamera.detect.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;

import com.ipcamer.detect.R;

public class DrawCaptureRect extends View
{
    private int mColorFill;//用于绘制的颜色
    private int mleft, mtop, mwidth, mheight;//矩形框的左边，顶部，宽，高

    public DrawCaptureRect(Context context, int left, int top, int width, int height, int colorFill) {
        super(context);
        // TODO Auto-generated constructor stub
        this.mColorFill = colorFill;
        this.mleft = left;
        this.mtop = top;
        this.mwidth = width;
        this.mheight = height;
    }
    /*
    setParams 因为人脸检测输出的矩形位置和视频的布局位置有缩放和高度不同的问题，需要进一步设置矩形框的参数
    faceRect: Rect 类
    height1: 顶部的文字框的高度
    height2: 视频布局到顶部的距离
    */
    public void setParams(Rect faceRect, int height1, int height2) {
        mleft = faceRect.left;
        //mtop高度下降
        mtop = faceRect.top + height1 + height2;
        mwidth = faceRect.width();
        //矩形框高度按增大
        mheight = (int)(faceRect.height() * 1.2);
    }



    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        Paint mpaint = new Paint();
        mpaint.setColor(mColorFill);
        mpaint.setStyle(Paint.Style.FILL);
        mpaint.setStrokeWidth(2.0f);
        canvas.drawLine(mleft, mtop, mleft+mwidth, mtop, mpaint);
        canvas.drawLine(mleft+mwidth, mtop, mleft+mwidth, mtop+mheight, mpaint);
        canvas.drawLine(mleft, mtop, mleft, mtop+mheight, mpaint);
        canvas.drawLine(mleft, mtop+mheight, mleft+mwidth, mtop+mheight, mpaint);
        super.onDraw(canvas);
    }
}

